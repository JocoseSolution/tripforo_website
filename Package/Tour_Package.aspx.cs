﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
public partial class Package_Tour_Package : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString());

    protected void Page_Load(object sender, EventArgs e)
    {
        string type = Request.QueryString["type"];


        if ((type != "Nothing") && (type == "Domestic"))

        {
            BindAllDomesticPackage();
        }
        else if ((type != "Nothing") && (type == "International"))
        {
            BindAllInternationalPackage();
        }

        else if ((type != "Nothing") && (type == "All"))
        {
            BindAllPackage();
        }

    }
    protected void BindAllPackage()
    {
        try
        {
            con.Open();
            string sp = "SELECT * FROM pkg_Details WHERE Pkg_status='1'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            // cmd.Parameters.AddWithValue("@pkgid", packgId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PkgDetails.DataSource = dt;
            PkgDetails.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            TypePkg.Text = "All ";
            con.Close();
        }


        catch (Exception ex)
        {
            con.Close();

            // clsErrorLog.LogInfo(ex);
        }
    }

    protected void BindAllDomesticPackage()
    {
        try
        {
            con.Open();
            string sp = "SELECT * FROM pkg_Details WHERE pkg_type='Domestic' AND pkg_status='1'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            // cmd.Parameters.AddWithValue("@pkgid", packgId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PkgDetails.DataSource = dt;
            PkgDetails.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            TypePkg.Text = "Domestic";
            con.Close();
        }


        catch (Exception ex)
        {
            con.Close();

            // clsErrorLog.LogInfo(ex);
        }
    }
    protected void BindAllInternationalPackage()
    {
        try
        {
            con.Open();
            string sp = "SELECT * FROM pkg_Details WHERE pkg_type='International' AND pkg_status='1'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            // cmd.Parameters.AddWithValue("@pkgid", packgId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PkgDetails.DataSource = dt;
            PkgDetails.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            TypePkg.Text = "International";
            con.Close();
        }


        catch (Exception ex)
        {
            con.Close();

            // clsErrorLog.LogInfo(ex);
        }
    }
    protected void OnSelectedIndexChanged(object sender, EventArgs e)
    {
        string sort = ddlSort.SelectedItem.Value;
        string type = Request.QueryString["type"];
        if (sort == "Popularity")
        {
            if(type=="All")
            {
                String sp = " SELECT TOP 10 * from pkg_Details WHERE pkg_status='1'";
                SortPackage(sp, type);
            }
            else
            {
                String sp = " SELECT TOP 5 * from pkg_Details WHERE pkg_type='" + type + "'";
                SortPackage(sp, type);
            }
           
        }
        else if (sort == "low_High")
        {
            if(type=="All")
            {
                String sp = " select * from pkg_Details  ORDER BY pkg_price ASC";
                SortPackage(sp, type);
            }
            else
            {
                String sp = " select * from pkg_Details where pkg_type ='" + type + "' ORDER BY pkg_price ASC";
                SortPackage(sp, type);
            }
            
        }
        else if (sort == "High_Low")
        {
            if(type=="All")
            {
                String sp = " select * from pkg_Details ORDER BY pkg_price DESC";
                SortPackage(sp, type);
            }
            else
            {
                String sp = " select * from pkg_Details where pkg_type ='" + type + "' ORDER BY pkg_price DESC";
                SortPackage(sp, type);
            }
          
        }
        
        // ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + message + "');", true);
    }

    protected void SortPackage(String sp, string type)
    {
        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            // cmd.Parameters.AddWithValue("@pkgid", packgId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PkgDetails.DataSource = dt;
            PkgDetails.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            TypePkg.Text = type;
            con.Close();
        }


        catch (Exception ex)
        {
            con.Close();

            // clsErrorLog.LogInfo(ex);
        }
    }
}