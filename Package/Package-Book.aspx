﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="Package-Book.aspx.cs" Inherits="Package_Package_Book" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <style type="text/css">
        span {
            font-family: 'Poppins' !important;
        }

        p {
            line-height: 1.9 !important;
            text-align: justify !important;
        }
        li{
         text-align: justify !important;

        }
        table {
            width: 100% !important;
            text-align: justify !important;
        }

        #main-wrapper {
            background: url(https://pngimage.net/wp-content/uploads/2018/06/schnee-png-3.png) !important;
            background-color: #fff !important;
        }

        .main-box-holid {
            margin: 0px;
            width: 100%;
            margin: 0px;
            padding: 0px 0 35px 0;
            border-bottom: 1px solid #bcbcbc;
            display: table;
        }

        .main-lef-bx {
            width: 48%;
            padding: 0px;
            margin: 0px;
            float: left;
            border: 1px solid #d6d7d8;
        }

            .main-lef-bx h3 {
                margin: 0px;
                padding: 5px;
                border-bottom: 1px solid #d6d7d8;
                font-size: 15px;
                background: #f6f6f6;
            }

            .main-lef-bx ul {
                margin: 0px;
                padding: 10px 0 15px 22px;
            }

            .main-lef-bx li {
                margin: 0px;
                font-size: 12px;
                padding: 0 0 0 0;
                color: #696969;
            }

        .main-rig-bx {
            width: 48%;
            padding: 0px;
            margin: 0px;
            float: right;
            border: 1px solid #d6d7d8;
        }

            .main-rig-bx h3 {
                margin: 0px;
                font-size: 15px;
                padding: 5px;
                border-bottom: 1px solid #d6d7d8;
                background: #f6f6f6;
            }

            .main-rig-bx ul {
                margin: 0px;
                padding: 10px 0 15px 22px;
            }

            .main-rig-bx li {
                margin: 0px;
                font-size: 12px;
                padding: 0 0 0 0;
                color: #696969;
            }


        .tracked {
            width: 100%;
            float: left;
            margin: 0;
            padding: 0 0 15px 0;
        }

        .main-new-days {
            margin: 0px;
            padding: 0px;
            width: 100% !important;
        }

        .bor-main-new {
            border-bottom: 1px solid #bcbcbc;
            padding: 0 0 0 0;
            width: 100%;
            float: left;
            margin: 0 0 13px 0;
        }

        .mn-dot-d-cire {
            float: left;
            margin: 0px;
            width: 100%;
        }

        .main-new-days h3 {
            margin: 14px 0;
        }

        div.container2 {
            display: flex;
            flex: auto;
            flex-direction: column;
            max-height: 100%;
            margin-bottom: 10px;
        }

        .container2 h4 {
            margin: 0 0 10px 0;
            text-align: center;
            font-size: 14px;
            width: 10.5%;
            background: #ff5342;
            color: #fff;
            padding: 4px;
            border-radius: 10px;
        }

        div.item {
            display: flex;
            flex: auto;
            padding: 0;
        }

        .timeline {
            position: relative;
            display: table;
            height: 100%;
            margin: 0;
            padding: 0;
            width: 100%;
        }

            .timeline section.year {
                position: relative;
            }

        @media (min-width: 62em) {
            .timeline h3 {
                font-size: 1.1em;
            }
        }

        .timeline h3 {
            position: -webkit-sticky;
            position: relative;
            top: 30px;
            color: #000;
            margin: 0;
            font-size: 15px;
            font-weight: 400;
        }

        .main-new-days h3 {
            margin: 14px 0;
        }

        .timeline section.year:first-child section {
            margin-top: -1.3em;
            padding-bottom: 0px;
        }

        .timeline section.year section {
            position: relative;
            margin-bottom: 2.2em;
        }

        .cr_icn {
            width: 43px;
            height: 28px;
            float: left;
            margin: 0 10px 10px 105px;
            color: #919191;
        }

        .timeline div:after {
            content: '';
            width: 2px;
            position: absolute;
            top: 0.5rem;
            bottom: 0rem;
            left: 80px;
            z-index: 1;
            background: #C5C5C5;
        }

        .timeline section.year section ul:last-child {
            margin-bottom: 0;
        }

        @media (min-width: 62em) {
            .timeline section.year section ul {
                font-size: 1.1em;
                padding: 0 0 0 101px;
            }
        }

        .timeline section.year section ul {
            list-style-type: none;
            padding: 0 0 0 75px;
            margin: -1.35rem 0 1em;
            max-width: 40rem;
            font-size: 1em;
        }

            .timeline section.year section ul li {
                margin-left: 9.5rem;
                font-size: 13px;
            }

            .timeline section.year section ul:first-of-type:after {
                content: '';
                width: 15px;
                height: 15px;
                background: #C5C5C5;
                border: 2px solid #FFFFFF;
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                -ms-border-radius: 50%;
                border-radius: 50%;
                position: absolute;
                left: 73px;
                top: 10px;
                z-index: 2;
            }

        .si_icn {
            width: 42px;
            height: 28px;
            margin: 0 10px 10px 105px;
            float: left;
            color: #919191;
        }

        .ht_icn {
            width: 43px;
            height: 29px;
            margin: 0 10px 10px 105px;
            float: left;
            color: #919191;
        }

        .br_icn {
            width: 33px;
            height: 28px;
            margin: 0 19px 10px 105px;
            float: left;
            color: #919191;
        }


        div.bg-light .package-info-wrapper {
            color: #939393;
        }

        div.bg-light .package-info-wrapper {
            border-color: #ebebeb;
        }

        div.bg-light .package-info-wrapper {
            background-color: #f7f7f7;
        }

        div.bg-light .package-info-wrapper {
            border-width: 1px;
            border-style: solid;
            padding: 12px 20px;
            margin-bottom: 30px;
            font-size: 14px;
            position: relative;
        }

            div.bg-light .package-info-wrapper .package-info {
                padding: 3px 0;
            }

            div.bg-light .package-info-wrapper .head {
                font-weight: 700;
            }

        a.gdl-button.large.package-book-now-button.gdl-button {
            padding: 2px 15px;
            float: left;
        }

        div.bg-light .package-info-wrapper .package-book-now-button {
            margin-top: 0;
            margin-right: 0;
            position: absolute;
            right: 30px;
            margin-top: -20px;
            top: 50%;
        }

        a.gdl-button:hover {
            opacity: .8;
            filter: alpha(opacity=80);
            color: #000;
        }

        a.gdl-button.large {
            padding: 2px 20px;
            height: 33px;
            line-height: 26px;
            font-size: 15px;
        }

        a.bg-light, button, input[type=submit], input[type=reset], input[type=button] {
            color: #fff;
        }

        a.gdl-button, body button, input[type=submit], input[type=reset], input[type=button] {
            background-color: #fa1314;
            border: 1px solid #fa1314;
        }

        a:hover {
            color: #80acd6;
        }

        a.package-book-now-button {
            margin-top: 20px;
            margin-bottom: 0;
        }

        a.gdl-button {
            display: inline-block;
            cursor: pointer;
            padding: 0 15px;
            height: 30px;
            line-height: 30px;
            margin-bottom: 20px;
            margin-right: 10px;
            border-bottom-width: 3px;
            border-style: solid;
            font-size: 12px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-weight: 600;
            color: #fff;
        }

        .gdl-button, button, input[type=submit], input[type=reset], input[type=button] {
            border-color: #246096;
        }

        div.gdl-recent-post-widget, div.gdl-recent-port-widget {
            padding-top: 10px;
        }

        div.recent-post-widget {
            margin-bottom: 20px;
        }

            div.gdl-blog-list .blog-media-wrapper, div.recent-port-widget .recent-port-widget-thumbnail, div.recent-post-widget .recent-post-widget-thumbnail, div.custom-sidebar .flickr_badge_image {
                border-color: #e3e3e3;
            }

            div.gdl-blog-list .blog-media-wrapper, div.recent-port-widget .recent-port-widget-thumbnail, div.recent-post-widget .recent-post-widget-thumbnail, div.custom-sidebar .flickr_badge_image {
                background-color: #eee;
            }

            div.recent-post-widget .recent-post-widget-thumbnail {
                padding: 3px;
                float: left;
                margin-right: 13px;
                border-width: 1px;
                border-style: solid;
                /*max-width: 55px;*/
            }

            div.recent-post-widget .recent-post-widget-context {
                overflow: hidden;
            }

            div.recent-post-widget .recent-post-widget-title {
                font-size: 19px;
                font-weight: 400;
                padding-top: 2px;
                margin-bottom: 7px;
                line-height: 1.3;
            }

            div.recent-post-widget .recent-post-widget-info {
                font-size: 11px;
                letter-spacing: 1px;
                text-transform: uppercase;
                font-weight: 700;
                line-height: 17px;
            }

        .sidebar-wrapper .recent-post-widget-info, .sidebar-wrapper #twitter_update_list {
            color: #a5a5a5;
        }

        div.recent-post-widget .recent-post-widget-info i {
            margin-right: 4px;
            font-size: 15px;
            float: left;
        }

        div.package-info .normal-price {
            text-decoration: line-through;
            margin-right: 10px;
        }

        div.package-info .discount-text, div.package-info .discount-price, div.package-info .separator {
            color: #e9513c;
        }

        div.package-info .discount-text, div.package-info .discount-price, div.package-info .separator {
            color: #e9513c;
        }

        div.package-info .discount-text, div.package-info .discount-price, div.package-info .separator {
            color: #e9513c;
        }

        .d-block {
            display: inline !important;
        }

        .mySlides {
            display: none;
        }

        /* Add a pointer when hovering over the thumbnail images */
        .cursor {
            cursor: pointer;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 5%;
            width: auto;
            padding: 16px;
            background: #f9f7f782;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            margin-right: 39px;
            border-radius: 3px 0 0 3px;
        }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next:hover {
                background-color: rgba(0, 0, 0, 0.8);
            }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* Container for image text */
        .caption-container {
            text-align: center;
            background-color: #222;
            padding: 2px 16px;
            color: white;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Six columns side by side */
        .column {
            float: left;
            width: 16.66%;
        }

        /* Add a transparency effect for thumnbail images */
        .demo {
            opacity: 0.6;
        }

            .active,
            .demo:hover {
                opacity: 1;
            }
    </style>

    <%-- <style type="text/css">
        @media (min-width: 992px)
        {.col-lg-4 
        {
            -ms-flex: 0 0 33.333333%;
            flex: 9 0 40.333333%;
            max-width: 101.333333% !important;
        }
        }
    </style>--%>
    <style type="text/css">
        #slider {
            position: relative;
            width: 700px;
            height: 400px;
            overflow: hidden;
            box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
        }

            #slider ul {
                position: relative;
                list-style: none;
                height: 100%;
                width: 10000%;
                padding: 0;
                margin: 0;
                transition: all 750ms ease;
                left: 0;
            }

                #slider ul li {
                    position: relative;
                    height: 100%;
                    float: left;
                }

                    #slider ul li img {
                        width: 650px;
                        height: 600px;
                    }

            #slider #prev, #slider #next {
                width: 50px;
                line-height: 50px;
                text-align: center;
                color: white;
                text-decoration: none;
                position: absolute;
                top: 50%;
                border-radius: 50%;
                font-size: 2rem;
                text-shadow: 0 0 20px rgba(0, 0, 0, 0.6);
                transform: translateY(-50%);
                transition: all 150ms ease;
            }

            #slider #prev {
                left: 10px;
            }

            #slider #next {
                right: 10px;
            }

                #slider #prev:hover, #slider #next:hover {
                    background-color: rgba(0, 0, 0, 0.5);
                    text-shadow: 0;
                }
    </style>

    </head>
<body>
    <form id="form1" runat="server">
    <asp:Repeater ID="PkgDetails" runat="server">
        <ItemTemplate>

            <section class="page-header page-header-text-light bg-secondary" style="background: url(https://d1e4pidl3fu268.cloudfront.net/8a204b1c-83db-48e5-82df-4e7d16cd8b89/world_wonders.crop_1280x328_0,0.preview.png) no-repeat; background-position: right; background-size: contain;">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h1 class="text-6 mb-1 d-flex flex-wrap align-items-center"><%# Eval("pkg_Title")%>                               
                            </h1>
                            <p class="opacity-8 mb-0"><i class="icofont-beach"></i><%# Eval("pkg_theme")%></p>
                           <a href="Home_Package.aspx"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i> Home</spna></a>

                            <a href="Tour_Package.aspx?type=Domestic"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i> Domestic</spna></a>
                                <a href="Tour_Package.aspx?type=International"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i>International</spna></a>
                                <a href="Tour_Package.aspx?type=All"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i> All Packages</spna></a>
                        </div>
                    </div>
                </div>
            </section>


            <div id="content">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-8">
                            <div class="bg-light shadow-md rounded p-3 p-sm-4 confirm-details">
                                <div class="package-media-wrapper gdl-image">
                                    <%=ImageList %>
                                    <%--
                                        <div class="mySlides">
                                        <img src="https://www.w3schools.com/howto/img_woods_wide.jpg" style="width: 100%">
                                    </div>
                                    
                                    --%>
                                    <%if (IsImgPNShow)
                                        {%>
                                    <a class="prev" style="margin-top: 15px;" onclick="plusSlides(-1)">❮</a>
                                    <a class="next" style="margin-top: 15px;" onclick="plusSlides(1)">❯</a>
                                    <%} %>
                                </div>


                            


                                <nav id="navbar-example2" class="bg-light pb-2 mb-2 sticky-top smooth-scroll">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item"><a class="nav-link text-nowrap" href="#known-for">Overview</a> </li>
                                        <li class="nav-item" style='<%# Eval("Pkg_Hotel_Mark").ToString() == "True" ? "display:block": "display:none"%>'><a class="nav-link" href="#choose-room">Hotel</a> </li>
                                        <li class="nav-item"><a class="nav-link" href="#Inclusions">Itineary</a> </li>
                                        <li class="nav-item" style='<%# Eval("Pkg_Hotel_Mark").ToString() == "True" ? "display:block": "display:none"%>'><a class="nav-link" href="#hotel-policy">Hotel Policy</a> </li>
                                    </ul>
                                </nav>
                                <p id="known-for"><%# Eval("pkg_description")%> </p>

                                <div class="package-info-wrapper">
                                    <div class="package-info"><i class="icofont-clock-time"></i><span class="head">Duration: </span><%# Eval("pkg_startdate", "{0:dd/MMMM/yyyy}")%> &#10148; <%# Eval("pkg_enddate","{0:dd/MMMM/yyyy}")%></div>
                                    <div class="package-info"><i class="icofont-location-arrow"></i><span class="head">Location: </span><%# Eval("pkg_Location")%></div>
                                    <div class="package-info"><i class="icofont-ticket"></i><span class="head">Itenerary Days: </span><%# Eval("pkg_noofday") %> Days / <%# Eval("pkg_noofnight") %> Nights </div>

                                    <div class="package-info">
                                        <i class="icofont-tag"></i><span class="head">Price: </span>&nbsp;
                                        <%#Convert.ToBoolean(Eval("pkg_offerPrice")) == true ?"<del class='d-block'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</del>&nbsp;&nbsp;<span style='font-size:20px; color:red;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_Offer_price_value")), 2)+"</span>":"<span style='font-size:20px; color:red;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</span>"%>
                                    </div>

                                    <%--<%#Convert.ToBoolean(Eval("pkg_offerPrice")) == true ? "<del class='d-block'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</del><div class='text-dark text-7 font-weight-500 mr-2 mr-lg-3' style='color: red !important'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_Offer_price_value")), 2)+"</div>" : "<div class='text-dark text-7 font-weight-500 mr-2 mr-lg-3' style='color: red !important'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</div>" %>--%>


                                    <%--                                       <div class="package-info"><i class="icofont-tag"></i><span class="head">Price: </span>₹ <%# Eval("pkg_offerPrice").ToString() =="False" ? Convert.ToInt32(Eval("pkg_Offer_price_value")) : Convert.ToInt32(Eval("pkg_price")) %></div>--%>
                                    <div class="package-info">
                                    <a class="package-book-now-button gdl-button large various" href="#" data-toggle="modal" data-target="#book-packagae">Book Now!</a><div class="clear">

                                                                                                                                                                       </div>
                                                                                                                                                                       </div>

                                </div>
                                <h2 id="reviews" class="text-6 mb-3 mt-2">Day Wise Itineary </h2>

                                <div id="wayp-4" class="test3 tracked" style="width: 100%;">
                                    <div class="main-new-days">
                                        <div class="bor-main-new">
                                            <div class="mn-dot-d-cire">

                                                <div class="">
                                                    <div class="container2 ">
                                                        <div class="timeline">
                                                            <%--                                                        <%# Eval("Pkg_Itinerary")%>--%>
                                                            <%# ((string)Eval("Pkg_Itinerary")).Replace("Day 1", "<h4>Day 1</h4>").Replace("Day 2", "<h4>Day 2</h4>").Replace("Day 3", "<h4>Day 3</h4>").Replace("Day 4", "<h4>Day 4</h4>").Replace("Day 5", "<h4>Day 5</h4>").Replace("Day 6", "<h4>Day 6</h4>").Replace("Day 7", "<h4>Day 7</h4>").Replace("Day 8", "<h4>Day 8</h4>").Replace("Day 9", "<h4>Day 9</h4>").Replace("Day 10", "<h4>Day 10</h4>") %>
                                                        </div>
                                                    </div>

                                                    <%--  <div class="container2 ">
                                                <h4>Day 1</h4>
                                                <div class="item">

                                                    <div class="timeline">
                                                        <div>
                                                            <section class="year ">
                                                                    <h3>10:00:AM</h3>
                                                                    <section>
                                                                        <div class="cr_icn"><i class="icofont-van icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Arrival at Delhi airport.Get transfer to Manali</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>03:00:PM</h3>
                                                                    <section>
                                                                        <div class="ht_icn"><i class="icofont-5-star-hotel icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Check in to the hotel.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>06:00:PM</h3>
                                                                    <section>
                                                                        <div class="si_icn"><i class="icofont-binoculars icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li><b>Visit the Cellular Jail:</b> a place of pride in India's freedom struggle. Many a stalwart were imprisoned here. Declared a national memorial, this three-storey prison constructed by Britishers in 1906, is a pilgrimage destination for freedom fighters.

The day draws to a gentle close with the sound and light show at the historic Cellular Jail which gives you a vivid experience of its strong association with the freedom fighters and their struggle for independence. Overnight at the hotel.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>08:00:PM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Dinner at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>10:00:PM</h3>
                                                                    <section>
                                                                        <div class="ht_icn"><i class="icofont-5-star-hotel icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Overnight stay at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container2 ">
                                                <h4>Day 2</h4>
                                                <div class="item">

                                                    <div class="timeline">
                                                        <div>
                                                            <section class="year ">
                                                                    <h3>05:00:AM</h3>
                                                                    <section>
                                                                        <div class="si_icn"><i class="icofont-binoculars icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Early in the morning, you will enjoy jeep safari in the park.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>08:00:AM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Morning breakfast at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>04:00:PM</h3>
                                                                    <section>
                                                                        <div class="si_icn"><i class="icofont-binoculars icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Proceed to sightseeing tour .Visit : Hadimba Temple.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>06:00:PM</h3>
                                                                    <section>
                                                                        <div class="cr_icn"><i class="icofont-van icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>After complete sightseeing return to hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>08:00:PM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Dinner at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>10:00:PM</h3>
                                                                    <section>
                                                                        <div class="ht_icn"><i class="icofont-5-star-hotel icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Overnight stay at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container2 ">
                                                <h4>Day 3</h4>
                                                <div class="item">

                                                    <div class="timeline">
                                                        <div>
                                                            <section class="year ">
                                                                    <h3>08:00:AM</h3>
                                                                    <section>
                                                                        <div class="br_icn"><i class="icofont-lunch icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>Morning breakfast at hotel</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <section class="year ">
                                                                    <h3>11:00:AM</h3>
                                                                    <section>
                                                                        <div class="cr_icn"><i class="icofont-van icofont-3x"></i></div>
                                                                        <ul>
                                                                            <li>After breakfast check out the hotel and back to Delhi. On reaching you will be transferred to airport or railway station for your journey back home.</li>
                                                                        </ul>
                                                                    </section>
                                                                </section>
                                                            <div class="clr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>--%>
                                                </div>



                                                <div id="pdf" style="display: none">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <h2 id="Inclusions" class="text-6 mb-3 mt-2">Inclusions</h2>
                                <div id="wayp-4_Inclusions" class="test3 tracked" style="width: 100%;">
                                    <div class="main-new-days">
                                        <div class="bor-main-new">
                                            <div class="mn-dot-d-cire">
                                                <div class="">
                                                    <div class="container2 ">
                                                        <%# Eval("Pkg_Inclu")%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <h2 id="Exclusions" class="text-6 mb-3 mt-2">Exclusions</h2>
                                <div id="wayp-4_Exclusions" class="test3 tracked" style="width: 100%;">
                                    <div class="main-new-days">
                                        <div class="bor-main-new">
                                            <div class="mn-dot-d-cire">
                                                <div class="">
                                                    <div class="container2 ">
                                                        <%# Eval("Pkg_Execlu")%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div style='<%# Eval("Pkg_Hotel_Mark").ToString() == "True" ? "display:block": "display:none"%>'>
                                    <hr class="mb-4">
                                    <h2 id="choose-room" class="text-6 mb-4 mt-2">Hotel </h2>
                                    <div class="row">
                                        <div class="col-12 col-sm-3 text-center">
                                            <img id="image" src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hotelimage")%>" alt="N/A" style="width: 168px;" />

                                        </div>
                                        <div class="col-12 col-sm-9 text-center text-sm-left">
                                            <span id="star" class="text-2">

                                                <%#(Eval("hotelstar").ToString()) == "1" ? "<i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "2" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "3" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "4" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : ((Eval("hotelstar").ToString()) == "" ? "<i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i><i class='icofont-star text-warning'></i>" : "- - -"))))%>

                                            </span>
                                            <p class="font-weight-600 mb-1"><%# Eval("pkghotelname")%></p>
                                            <p class="font-weight-600 mb-1">Location :-<%# Eval("hoteaddress")%></p>
                                            <p><%# Eval("hoteldes")%></p>
                                            <hr>
                                        </div>
                                    </div>

                                    <%--                                <div class="row">
                                    <div class="col-12 col-sm-3 text-center">
                                        <img src="https://media-cdn.tripadvisor.com/media/photo-s/09/6c/fb/ef/sun-park-resort-manali.jpg" style="width: 168px;" />

                                    </div>
                                    <div class="col-12 col-sm-9 text-center text-sm-left">

                                        <p class="font-weight-600 mb-1"><%# Eval("hoteaddress")%></p>
                                        <p>The place is conveniently situated in the center of <%# Eval("hoteaddress")%> and is considered to be a great place to stay if guests want to explore the mountain regions further. Many travel attractions such as the Rohtang Pass, Pando Dam, Bijli Mahadev temple, Hidimba Devi temple, and Solang Valley are located near the place.</p>
                                        <hr>
                                    </div>
                                </div>--%>

                                    <div class="row">
                                        <div class="col-12 col-sm-3 text-center">
                                            <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hotelroomimgae")%>" alt="N/A" style="width: 168px;" />
                                        </div>
                                        <div class="col-12 col-sm-9 text-center text-sm-left">
                                            <p class="font-weight-600 mb-1">Rooms</p>
                                            <p><%# Eval("hotelroomdes")%></p>
                                            <hr>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-sm-3 text-center">
                                            <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hoteldiningimg")%>" alt="N/A" style="width: 168px;" />
                                        </div>
                                        <div class="col-12 col-sm-9 text-center text-sm-left">
                                            <p class="font-weight-600 mb-1">Dining</p>
                                            <p><%# Eval("hoteldiningdes")%></p>
                                            <hr>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-sm-3 text-center">
                                            <img src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("hotelamenimg")%>" alt="N/A" style="width: 168px;" />
                                        </div>
                                        <div class="col-12 col-sm-9 text-center text-sm-left">
                                            <p class="font-weight-600 mb-1">Business and other amenities</p>
                                            <p><%# Eval("hotelamendes")%></p>
                                            <%--                                <p>There is a meeting room that can be used for official events like meetings and conferences. The fax and free internet facilities will help the guests during these activities.</p>--%>
                                            <hr>
                                        </div>
                                    </div>



                                    <div class="d-flex align-items-center mt-3"><a href="#" data-toggle="modal" data-target="#cancellation-policy">Hotel Policy</a></div>

                                    <div id="cancellation-policy" class="modal fade" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Hotel Policy</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span> </button>
                                                </div>
                                                <div class="modal-body">
                                                    <ul>
                                                        <li><%# Eval("checkinpolicy")%></li>
                                                        <li><%# Eval("checkoutpolicy")%></li>

                                                        <%--<li>The standard check-in time is 12:00 PM and the standard check-out time is 11:00 AM. Early check-in or late check-out is strictly subjected to availability and may be chargeable by the hotel. Any early check-in or late check-out request must be directed and reconfirmed with hotel directly, Accommodation, 24 hours House Keeping, Hot and Cold water available, Internet, Telephone.</li>
                                            <li>Hotel Policy: Most hotels do not allow unmarried/unrelated couples to check-in. This is at the full discretion of the hotel management. No refund would be applicable in case the hotel denies check-in under such circumstances., Most hotels do not allow same city/local ID Proofs for check-in. Kindly check with your hotel about the same before checking in. This is at full discretion of the hotel management. No refund would be applicable in case the hotel denies check-in under such circumstances.</li>--%>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <hr class="my-4">
                                </div>







                                <!-- Hotel Policy
        	============================================= -->
                                <%--<h2 id="Inclusions" class="text-6 mb-3 mt-2">Package Policy</h2>
                                <div id="wayp-4_Inclusions" class="test3 tracked" style="width: 100%;">
                                    <div class="main-new-days">
                                        <div class="bor-main-new">
                                            <div class="mn-dot-d-cire">
                                                <div class="">
                                                    <div class="container2 ">
                                                      <%# Eval("Pkg_Policy")%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <h2 id="hotel-policy" class="text-6 mb-3 mt-2">Package Policy</h2>
                                <div class="container2 ">
                                    <%# Eval("PackagePolicy")%>
                                </div>

                            </div>
                        </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Middle Panel End-->
    <div id="book-packagae" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 30px !important;">
                <div class="modal-header">
                    <h5 class="modal-title" style="text-align: center !important;">Book Package</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span> </button>
                </div>
                <div class="modal-body">
                    <div class="enqform">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="text" id="input_name" class="form-control" data-bv-field="number" required="" placeholder="Full Name" />
                                <%--                            <asp:TextBox ID="txtFullName" runat="server" placeholder="Full Name" class="form-control"></asp:TextBox>--%>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" id="email" class="form-control" required="" placeholder="Email" />
                                <%--                            <asp:TextBox ID="txtEmailId" runat="server" placeholder="Email" class="form-control"></asp:TextBox>--%>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="mobile" required="" placeholder="Mobile No." />
                                <%--                            <asp:TextBox ID="txtMobNo" runat="server" placeholder="Mobile No." class="form-control"></asp:TextBox>--%>
                            </div>

                            <div class="form-group col-md-12">
                                <textarea class="form-control" required="" id="address" placeholder="Address"></textarea>
                                <%--                            <asp:TextBox ID="txtAddress" runat="server" placeholder="Address" TextMode="MultiLine" class="form-control"></asp:TextBox>--%>
                            </div>
                            <div class="form-group col-md-12">
                                <textarea class="form-control" id="notes" required="" placeholder="Additional Notes"></textarea>
                                <%--                            <asp:TextBox ID="txtAddNotes" runat="server" placeholder="Additional Notes" TextMode="MultiLine" class="form-control"></asp:TextBox>--%>
                            </div>
                            <div class="form-group col-md-12" style="text-align: center;">
                                <%--                            <button class="btn btn-primary btn-block" type="submit">Book Now</button>--%>
                                <%--                                                <button type="submit" id="Submit"  runat="server" class="btn btn-primary btn-block" onserverClick="Submit_Click"  >Submit</button>--%>
                                <span class="btn btn-primary" id="btnEnqSend" onclick="return SendEnqDetails();">Submit</span>

                            </div>
                        </div>

                    </div>

                    <div id="enqsent" style="display: none;">
                        <div class="mb-3" style="text-align: center; padding: 20px;">
                            <h5 id="hmessage"></h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Side Panel
        ============================================= -->
    <aside class="col-lg-4 mt-4 mt-lg-0" style="display: block;">
        <div class="bg-light shadow-md rounded p-3 sticky-top">
            <p class="hotels-reviews text-center"><span class="font-weight-600">LAST MINUTE DEALS</span></p>

            <div class="gdl-recent-post-widget">
                <asp:Repeater ID="LastMinutesPkgDetails" runat="server">
                    <ItemTemplate>
                        <div class="recent-post-widget">
                            <div class="recent-post-widget-thumbnail">
                                <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                    <img src="/HolidaysImage/<%# Eval("pkg_Image") %>" alt=""  srcset="" style="width: 55px;" />

                                </a>

                            </div>
                            <div class="recent-post-widget-context">
                                <h4 class="recent-post-widget-title"><a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>"><%# Eval("pkg_Title")%> </a></h4>
                                <div class="recent-post-widget-info">
                                    <div class="recent-post-widget-date">
                                        <div class="package-info">
                                            <i class="icon"></i><span class="normal-price">₹<%# Convert.ToInt32(Eval("pkg_price")) %></span>
                                            <%--                                            <div style='<%# Eval("pkg_offerPrice").ToString() == "True" ? "display:block" : "display:none"%>'>--%>
                                            <span class="discount-text"><%# Eval("Pkg_Offer") %>% Off</span><span class="separator"> : </span>
                                            <span class="discount-price">₹<%# Convert.ToInt32(Eval("pkg_Offer_price_value")) %></span>
                                            <%--                                                </div>--%>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="clear"></div>

                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

            <p class="text-center mt-3 mb-1"><span class="text-uppercase font-weight-700">Checkin</span> : 12:00 PM / <span class="text-uppercase font-weight-700">Checkout</span> : 11:00 AM</p>
            <p class="text-danger text-center mb-1"><i class="far fa-clock"></i>Last Booked - 18 hours ago</p>
        </div>
    </aside>
    <!-- Side Panel End -->
    </div>
        </div>
    </div>

     <script type="text/javascript" src="../js/custom.js"></script>
    <%--    <script type="text/javascript" src="../js/manual.js"></script>--%>
    <script src="../js/jquery-3.6.0.min.js"></script>
    <script src="../js/popper/popper.min.js"></script>
    <script src="../js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript">
        CallByDuty = () => {
            $(".enqform").css("display", "block");
            $("#enqsent").css("display", "none");
            $("#hmessage").html();
            $("#input_name").css('border-color', '#eeeeee').val("");
            $("#email").css('border-color', '#eeeeee').val("");
            $("#mobile").css('border-color', '#eeeeee').val("");
            $("#address").css('border-color', '#eeeeee').val("");
            $("#notes").css('border-color', '#eeeeee').val("");
            $("#btnEnqSend").html("Send Massage<i class='fas fa-arrow-right ps-3'></i>");
        }
        SendEnqDetails = () => {
            if (ValidateForm1()) {
                let txt_name = $("#input_name").val();
                let txt_email = $("#email").val();
                let txt_mobile = $("#mobile").val();
                let txt_addresss = $("#address").val();
                let txt_notes = $("#notes").val();
                const params = new URLSearchParams(window.location.search);
                let pkgid = params.get('Pkg_Id');

                $("#btnEnqSend").html("Sending...<i class='fa fa-spinner fa-pulse'></i>");

                $.ajax({
                    type: "Post",
                    url: "/Package/Package-Book.aspx/SendEmailFromPortal",
                    data: '{name: ' + JSON.stringify(txt_name) + ',mobile: ' + JSON.stringify(txt_mobile) + ',email: ' + JSON.stringify(txt_email) + ',address: ' + JSON.stringify(txt_addresss) + ', notes: ' + JSON.stringify(txt_notes) + ',pkgid: ' + JSON.stringify(pkgid) + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        debugger;
                        if (response.d != null && response.d != "") {
                            if (response.d == "success") {
                                $(".enqform").css("display", "none");
                                $("#enqsent").css("display", "block");
                                $("#hmessage").html("Thanks for booking us! We will be in touch with you shortly.").css("color", "green");
                            }
                            else {
                                $(".enqform").css("display", "none");
                                $("#enqsent").css("display", "block");
                                $("#hmessage").html("oops ! something is wrong").css("color", "red");
                            }
                        }
                    }
                });
            }
        }

        function ValidateForm1() {

            if ($("#input_name").val() == "") {
                $("#input_name").focus().css('border-color', '#ef3139');
                return false;
            }
            if ($("#email").val() == "") {
                $("#email").focus().css('border-color', '#ef3139');
                return false;
            }
            if ($("#mobile").val() == "") {
                $("#mobile").focus().css('border-color', '#ef3139');
                return false;
            }
            if ($("#address").val() == "") {
                $("#address").focus().css('border-color', '#ef3139');
                return false;
            }

            return true;
        }
    </script>
    <script type="text/javascript">
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) { slideIndex = 1 }
            if (n < 1) { slideIndex = slides.length }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
            captionText.innerHTML = dots[slideIndex - 1].alt;
        }
    </script>

    </form>
</body>
</html>