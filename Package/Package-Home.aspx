﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Package-Home.aspx.cs" Inherits="Package_Package_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .card-img-top {
            width: 100%;
            border-top-left-radius: calc(.25rem - 1px);
            border-top-right-radius: calc(.25rem - 1px);
            height: 200px;
        }

        div.package-ribbon-wrapper {
            position: absolute;
            right: -8px;
            bottom: 155px;
        }

        .package-type.last-minute {
            background-color: #ff634d;
            color: #fff;
        }

        div.package-type {
            padding: 9px 12px;
            font-size: 14px;
            line-height: 1;
        }

            div.package-type span.head {
                font-size: 12px;
                display: block;
                line-height: 1;
                margin-bottom: 4px;
            }

            div.package-type span.discount-text {
                font-size: 18px;
                display: block;
                line-height: 1;
            }

        div.package-type-gimmick {
            width: 0;
            height: 0;
            float: right;
            border-width: 8px 8px 0 0;
            border-style: solid;
            border-bottom-color: transparent;
            border-right-color: transparent;
            border-left-color: transparent;
        }

        .package-type-gimmick {
            border-top-color: #000;
        }

        div.card .package-title-wrapper {
            position: absolute;
            bottom: 60px;
            left: 10px;
            right: 10px;
            padding: 8px 10px;
        }

        div.card .package-title-overlay {
            background: #000000ad;
            opacity: .8;
            filter: alpha(opacity=80);
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            top: 0;
            border-radius: 5px;
        }

        div.card .package-title {
            font-size: 18px;
            margin-top: -1px;
            margin-bottom: 0;
            float: left;
            position: relative;
            line-height: 20px;
        }

            div.card .package-title a {
                color: #fff;
            }

        div.card .package-info {
            font-size: 14px;
            line-height: 20px;
            font-weight: 700;
            float: right;
            position: relative;
        }


        div.card .package-price {
            color: #ff0303;
            font-weight: normal;
        }

        div.card .package-date {
            margin-bottom: 8px;
            font-size: 12px;
            margin-top: 8px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-weight: 700;
            text-align: center;
        }

            div.card .package-date i {
                margin-right: 7px;
                font-size: 15px;
            }
    </style>

    
    <style type="text/css">
        svg.intro {
  max-width: 100%;
  position: absolute;
  top: 40%;
  left: 35%;
  transform: translate(-50%, -50%);
}
svg.intro .text {
  display: none;
}
svg.intro.go .text {
  font-family: Arial, sans-serif;
  font-size: 20px;
  text-transform: uppercase;
  display: block;
}
svg.intro.go .text-stroke {
  fill: none;
  stroke: #f7980e;
  /*stroke-width: 2.8px;*/
  stroke-dashoffset: -900;
  stroke-dasharray: 900;
  stroke-linecap: butt;
  stroke-linejoin: round;
  -webkit-animation: dash 2.5s ease-in-out forwards;
          animation: dash 2.5s ease-in-out forwards;
}
svg.intro.go .text-stroke:nth-child(2) {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s;
}
svg.intro.go .text-stroke:nth-child(3) {
  -webkit-animation-delay: 0.9s;
          animation-delay: 0.9s;
}
svg.intro.go .text-stroke-2 {
  stroke: #fff;
  -webkit-animation-delay: 1.2s;
          animation-delay: 1.2s;
}
svg.intro.go .text-stroke:nth-child(5) {
  -webkit-animation-delay: 1.5s;
          animation-delay: 1.5s;
}
svg.intro.go .text-stroke:nth-child(6) {
  -webkit-animation-delay: 1.8s;
          animation-delay: 1.8s;
}

@-webkit-keyframes dash {
  100% {
    stroke-dashoffset: 0;
  }
}

@keyframes dash {
  100% {
    stroke-dashoffset: 0;
  }
}
.reload {
  position: absolute;
  bottom: 15px;
  right: 15px;
  background: #fff;
  border: none;
  border-radius: 20px;
  outline: none !important;
  font-size: 11px;
  line-height: 1.5;
  padding: 8px 12px;
  text-transform: uppercase;
  z-index: 10;
  cursor: pointer;
  box-shadow: 0 6px 7px #350e4c;
  transition: all 0.1s cubic-bezier(0.67, 0.13, 0.1, 0.81);
}
.reload:hover {
  box-shadow: 0 4px 4px #350e4c;
  transform: translateY(1px);
}
.reload:active {
  box-shadow: 0 1px 2px #244B94;
  transform: translateY(2px);
}
.reload svg {
  vertical-align: middle;
  position: relative;
  top: -2px;
}
    </style>

  <script type="text/javascript">
      $(function () {
          $('.intro').addClass('go');

          $('.reload').click(function () {
              $('.intro').removeClass('go').delay(200).queue(function (next) {
                  $('.intro').addClass('go');
                  next();
              });

          });
      })
  </script>

    <div class="bg-secondary">
        <div class="container">
            <ul class="nav secondary-nav">
                <li class="nav-item"><a class="nav-link active" href="Package/Tour_Package.aspx"><span><i class="icofont-beach"></i></span>Package</a> </li>
                <li class="nav-item"><a class="nav-link" href="booking-hotels.html"><span><i class="icofont-hotel"></i></span>Hotels</a></li>
                <li class="nav-item"><a class="nav-link " href="booking-flights.html"><span><i class="icofont-airplane"></i></span>Flights</a> </li>

                <li class="nav-item"><a class="nav-link" href="booking-bus.html"><span><i class="icofont-bus"></i></span>Bus</a> </li>
            </ul>
        </div>
    </div>



 


    <div id="content">

        <div class="row">
      <div class="col-lg-12">
        <div class="hero-wrap">
          <div class="hero-mask rounded opacity-7 bg-secondary"></div><%--<video src="../images/videos/pexels-vlada-karpovich-7429438.mp4" controls="controls" />--%>
          <div class="hero-bg rounded" style="position: relative;width: 100%;height: 100vh;"><video autoplay loop muted class="wrapper__video" style="width: 100%;min-width: 100%;min-height: 100vh;z-index: 1;"><source src="../images/videos/pexels-kampus-production-6181049.mp4"></video>  </div>
             
            <svg class="intro" viewbox="0 0 200 90">
  <text text-anchor="start" x="10" y="30" class="text text-stroke" clip-path="url(#text1)">Explore </text>
  <text text-anchor="start" x="10" y="50" class="text text-stroke" clip-path="url(#text2)">The</text>
  <text text-anchor="start" x="10" y="70" class="text text-stroke" clip-path="url(#text3)">World</text>
  <text text-anchor="start" x="10" y="90" class="text text-stroke" clip-path="url(#text4)">With Faremonster.</text>
  <text text-anchor="start" x="10" y="30" class="text text-stroke text-stroke-2" clip-path="url(#text1)">Explore</text>
  <text text-anchor="start" x="10" y="50" class="text text-stroke text-stroke-2" clip-path="url(#text2)">The</text>
  <text text-anchor="start" x="10" y="70" class="text text-stroke text-stroke-2" clip-path="url(#text3)">World</text>
  <text text-anchor="start" x="10" y="90" class="text text-stroke text-stroke-2" clip-path="url(#text4)">With Faremonster.</text>
  <defs>
    <clipPath id="text1">
      <text text-anchor="start" x="10" y="30" class="text">Explore</text>
    </clipPath>
    <clipPath id="text2">
      <text text-anchor="start" x="10" y="50" class="text">The</text>
    </clipPath>
    <clipPath id="text3">
      <text text-anchor="start" x="10" y="70" class="text">World</text>
    </clipPath>
       <clipPath id="text4">
      <text text-anchor="start" x="10" y="90" class="text">With Faremonster.</text>
    </clipPath>
  </defs
</svg>
<div>

</div>

         <%-- <div class="hero-content" style="position: absolute;z-index: 9999;top: 142px;padding: 20px;">
            <h2 class="text-6 text-white mb-3">Why Faremonster</h2>
            <p class="text-light mb-4">Fare Monster Travel Pvt. Ltd. look forward to taking you through amazing corporate travel experiences!</p>
            <h2 class="text-6 text-white mb-3">Booking Wedding Events with Faremonster</h2>
            <p class="text-light">From our initial call, right through to the emotional farewells; we're here to guide, source &amp; coordinate every aspect of your wedding celebrations:</p>

          </div>--%>
        </div>
      </div>
      
    </div>



        <section class="section bg-light">
    <div class="container">
      <h2 class="text-6 font-weight-500 mb-0">Featured Offers</h2>
      <p class="text-3">Get Best Offers &amp; Discounts</p>
      <div class="row">
          <div class="col-md-6">
            <a href="Tour_Package.aspx">
              <img class="img-fluid rounded" src="/images/slider/booking-banner-8.jpg" alt="banner 8"> </a>
          </div>
          <div class="col-md-6">
            <a href="Tour_Package.aspx">
              <img class="img-fluid rounded" src="/images/slider/booking-banner-7.jpg" alt="banner 7"> </a>
          </div>
        </div>
        </div>
      <div class="container mt-5 py-2">
        <h2 class="text-6 font-weight-500 mb-0">Popular Destinations</h2>
        <p class="text-3">Explore these places and some other thing here</p>
    
          <div class="hotels-list">
          <div class="row">
          <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="Tour_Package.aspx"><img src="https://www.taxiworld.in/wp-content/uploads/2020/01/beas-river.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>

                  <div class="package-title-wrapper">
                      <div class="package-title-overlay"></div>
                      <h2 class="package-title"><a href="https://demo.goodlayers.com/tourpackage/package/paris-and-bordueax/">Delhi - Manali</a></h2>
                      <div class="package-info"><i class="icon-tag"></i>
                          <span class="package-price">₹ 8,600</span>

                      </div>

                  </div>

                  <div class="package-date"><i class="icofont-clock-time"></i>11 Nov 2013 - 22 Nov 2013</div>
              </a>
              
              
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="Tour_Package.aspx"><img src="https://i.guim.co.uk/img/media/a9edc9006f74a5bd9760a7da4b5998dffd0ba3f7/1272_1_3436_2062/master/3436.jpg?width=445&quality=45&auto=format&fit=max&dpr=2&s=9205244d360c4e7848e86634cdf9f6b3" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>

                  <div class="package-title-wrapper">
                      <div class="package-title-overlay"></div>
                      <h2 class="package-title"><a href="https://demo.goodlayers.com/tourpackage/package/paris-and-bordueax/">Delhi - Nepal</a></h2>
                      <div class="package-info"><i class="icon-tag"></i>
                          <span class="package-price">₹ 8,600</span>

                      </div>

                  </div>

                  <div class="package-date"><i class="icofont-clock-time"></i>11 Nov 2013 - 22 Nov 2013</div>
              </a>
              
              
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="Tour_Package.aspx"><img src="https://images.moneycontrol.com/static-mcnews/2021/04/Mumbai-high-Rise-673x435.jpg?impolicy=website&width=770&height=431" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>

                  <div class="package-title-wrapper">
                      <div class="package-title-overlay"></div>
                      <h2 class="package-title"><a href="https://demo.goodlayers.com/tourpackage/package/paris-and-bordueax/">Delhi - Mumbai</a></h2>
                      <div class="package-info"><i class="icon-tag"></i>
                          <span class="package-price">₹ 8,600</span>

                      </div>

                  </div>

                  <div class="package-date"><i class="icofont-clock-time"></i>11 Nov 2013 - 22 Nov 2013</div>
              </a>
              
              
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="Tour_Package.aspx"><img src="https://res.cloudinary.com/dwzmsvp7f/image/fetch/q_75,f_auto,w_1316/https%3A%2F%2Fmedia.insider.in%2Fimage%2Fupload%2Fc_crop%2Cg_custom%2Fv1581685165%2Fs4ttjwmxzht5gemp4c4x.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>

                  <div class="package-title-wrapper">
                      <div class="package-title-overlay"></div>
                      <h2 class="package-title"><a href="https://demo.goodlayers.com/tourpackage/package/paris-and-bordueax/">Delhi - Ladakh</a></h2>
                      <div class="package-info"><i class="icon-tag"></i>
                          <span class="package-price">₹ 8,600</span>

                      </div>

                  </div>

                  <div class="package-date"><i class="icofont-clock-time"></i>11 Nov 2013 - 22 Nov 2013</div>
              </a>
              
              
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="Tour_Package.aspx"><img src="https://www.worldatlas.com/upload/70/33/ab/shutterstock-115227475.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>

                  <div class="package-title-wrapper">
                      <div class="package-title-overlay"></div>
                      <h2 class="package-title"><a href="https://demo.goodlayers.com/tourpackage/package/paris-and-bordueax/">Delhi - Kashmir</a></h2>
                      <div class="package-info"><i class="icon-tag"></i>
                          <span class="package-price">₹ 8,600</span>

                      </div>

                  </div>

                  <div class="package-date"><i class="icofont-clock-time"></i>11 Nov 2013 - 22 Nov 2013</div>
              </a>
              
              
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="Tour_Package.aspx"><img src="https://www.tourmyindia.com/states/uttarakhand/image/nainital-lake-view.webp" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>

                  <div class="package-title-wrapper">
                      <div class="package-title-overlay"></div>
                      <h2 class="package-title"><a href="https://demo.goodlayers.com/tourpackage/package/paris-and-bordueax/">Delhi - Nainital</a></h2>
                      <div class="package-info"><i class="icon-tag"></i>
                          <span class="package-price">₹ 8,600</span>

                      </div>

                  </div>

                  <div class="package-date"><i class="icofont-clock-time"></i>11 Nov 2013 - 22 Nov 2013</div>
              </a>
              
              
            </div>
          </div>

              <div class="col-md-4">
            <div class="card shadow-md border-0 mb-4">
              <a href="Tour_Package.aspx"><img src="https://res.cloudinary.com/dwzmsvp7f/image/fetch/q_75,f_auto,w_1316/https%3A%2F%2Fmedia.insider.in%2Fimage%2Fupload%2Fc_crop%2Cg_custom%2Fv1570484918%2Fvdqoa05uvrw3493v1uwr.jpg" class="card-img-top d-block" alt="...">
                  <div class="package-ribbon-wrapper">
                      <div class="package-type last-minute">
                          <span class="head">Last Minute</span>
                          <span class="discount-text">20% Off</span>

                      </div>
                      <div class="clear"></div>
                      <div class="package-type-gimmick"></div>
                      <div class="clear"></div>

                  </div>

                  <div class="package-title-wrapper">
                      <div class="package-title-overlay"></div>
                      <h2 class="package-title"><a href="https://demo.goodlayers.com/tourpackage/package/paris-and-bordueax/">Delhi - Auli</a></h2>
                      <div class="package-info"><i class="icon-tag"></i>
                          <span class="package-price">₹ 8,600</span>

                      </div>

                  </div>

                  <div class="package-date"><i class="icofont-clock-time"></i>11 Nov 2013 - 22 Nov 2013</div>
              </a>
              
              
            </div>
          </div>
   
          </div>
          </div>
      </div>
            
      <div class="container mt-5 py-3">
      <h2 class="text-6 font-weight-500 mb-0">Why Choose Us</h2>
        <p class="text-3">Book Hotels Online. Save Time and Money!</p>
        <div class="row">
          <div class="col-md-6 col-lg-4">
            <div class="featured-box border bg-light rounded pr-2 style-3">
              <div class="featured-box-icon h-100 border-right"> <i class="icofont-users-social"></i> </div>
              <h4 class="mt-3">10M+ Happy Customers</h4>
              <p class="text-muted">Book with one of most trusted travel portals in the world</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mt-3 mt-md-0">
            <div class="featured-box border bg-light rounded pr-2 style-3">
              <div class="featured-box-icon h-100 border-right"> <i class="icofont-tags"></i> </div>
              <h4 class="mt-3">Lowest Price Guarnteee</h4>
              <p class="text-muted">Always get lowest price with the best in the industry.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mt-3 mt-md-0">
            <div class="featured-box border bg-light rounded pr-2 style-3">
              <div class="featured-box-icon h-100 border-right"> <i class="icofont-live-support"></i> </div>
              <h4 class="mt-3">24X7 Customer Support</h4>
              <p class="text-muted">We're here to help. Round the clock support for all your hotel needs</p>
            </div>
          </div>
        </div>
      </div>
    </section>

 

    </div>

</asp:Content>

