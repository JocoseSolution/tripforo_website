﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="Tour_Package.aspx.cs" Inherits="Package_Tour_Package" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../Advance_CSS/css/styles.css" rel="stylesheet" />

     <style type="text/css">
        div.package-ribbon-wrapper {
            position: absolute;
            right: -8px;
            bottom: 223px;
        }

        .package-type.last-minute {
            background-color: #ff634d;
            color: #fff;
        }

        div.package-type {
            padding: 9px 12px;
            font-size: 14px;
            line-height: 1;
        }

            div.package-type span.head {
                font-size: 12px;
                display: block;
                line-height: 1;
                margin-bottom: 4px;
            }

            div.package-type span.discount-text {
                font-size: 18px;
                display: block;
                line-height: 1;
            }

        div.package-type-gimmick {
            width: 0;
            height: 0;
            float: right;
            border-width: 8px 8px 0 0;
            border-style: solid;
            border-bottom-color: transparent;
            border-right-color: transparent;
            border-left-color: transparent;
        }

        .package-type-gimmick {
            border-top-color: #000;
        }


    </style>
    <style type="text/css">
        @media (min-width: 768px) {
            .col-md-4 {
                -ms-flex: 0 0 33.333333%;
                flex: 0 0 33.333333%;
                max-width: 33.333333%;
            }
        }
    </style>
    </head>
<body>

    <form id="form1" runat="server">
     <nav class="navbar navbar-default navbar-inverse navbar-theme navbar-theme-abs navbar-theme-transparent navbar-theme-border" id="main-nav">
        <div class="container">
            <div class="navbar-inner nav">
                <div class="navbar-header">
                    
                    <a class="navbar-brand" href="/Login.aspx">
                        <img src="../Advance_CSS/Icons/logo(ft).png" alt="tripforo" title="Tripforo" />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main">
                    <ul class="nav navbar-nav">
                       <li>
                            <a href="Package/Home_Package.aspx">Package</a>

                        </li>
                        <li>
                            <a href="about-us.aspx">About</a>

                        </li>
                        <li>
                            <a href="contact-us.aspx">Contact</a>

                        </li>                       

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         
                       

                       
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    

    <div id="content" style="margin-top:106px;">
        <section class="container">

            <div class="row">



                <div class="col-lg-12 mt-4 mt-lg-0">

                    <div class="border-bottom mb-3 pb-3">
                        <div class="row align-items-center">
                            <div class="col-6 col-md-8">
                                <span class="mr-3"><span class="text-4">Package:</span> <asp:Label ID="TypePkg" runat="server" Text=""></asp:Label><span class="font-weight-600">
                                    <br />
                                    <asp:Label ID="Total" runat="server" Text=""></asp:Label> Packages</span> found</span> 
                                <a href="Home_Package.aspx"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i> Home</spna></a>

                                <a href="Tour_Package.aspx?type=Domestic"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i> Domestic</spna></a>
                                <a href="Tour_Package.aspx?type=International"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i>International</spna></a>
                                <a href="Tour_Package.aspx?type=All"><spna class="text-warning text-nowrap"><i class="icofont-hand-right"></i> All Packages</spna></a>
                            </div>
                            <div class="col-6 col-md-4">
                                <div class="row no-gutters ml-auto">
                                    <label class="col col-form-label-sm text-right mr-2 mb-0" for="input-sort">Sort By:</label>
                                   <%-- <select id="input-sort" class="custom-select custom-select-sm col">
                                        <option value="" selected="selected">Popularity</option>
                                        <option value="">Price - Low to High</option>
                                        <option value="">Price - High to Low</option>
                                        <option value="">User Rating - High to Low</option>
                                    </select>--%>
                                     <asp:DropDownList ID="ddlSort" CssClass="custom-select custom-select-sm col" runat="server"  AutoPostBack = "true" OnSelectedIndexChanged = "OnSelectedIndexChanged">
                                            <asp:ListItem Value="None">All</asp:ListItem>
                                            <asp:ListItem Value="Popularity">Popularity</asp:ListItem>
                                            <asp:ListItem Value="low_High">Price - Low to High</asp:ListItem>
                                            <asp:ListItem Value="High_Low">Price - High to Low</asp:ListItem>
                                            
                                        </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Sort Filter -->


                    <div class="hotels-list">
                        <div class="row">

                            <asp:Repeater ID="PkgDetails" runat="server">
                                <ItemTemplate>
                                    <div class="col-md-4">
                                        <div class="card shadow-md border-0 mb-4">
                                            <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>">
                                                <img src="/HolidaysImage/<%# Eval("pkg_Image") %>"  class="card-img-top d-block" alt="..." style="min-height: 204px;    max-height: 204px;" />
                                                
                                            </a>
                                            <div class="card-body">
                                                <h5><a href="#" class="text-dark text-5"><%# Eval("pkg_Title") %></a></h5>
                                                <p class="mb-2">
                                                    <span class="text-black-50"><%# Eval("pkg_noofday") %> Days / <%# Eval("pkg_noofnight") %> Nights</span>
                                                </p>
                                                <p class="hotels-amenities d-flex align-items-center mb-2 text-4">
                                                    <span data-toggle="tooltip" data-original-title="Internet/Wi-Fi">
                                                        <img src="../Advance_CSS/Icons/hotel.png" style="width: 35px;" /></span>
                                                    <span data-toggle="tooltip" data-original-title="Restaurant">
                                                        <img src="../Advance_CSS/Icons/flight.png" style="width: 35px;" /></span>
                                                    <span data-toggle="tooltip" data-original-title="Bar">
                                                        <img src="../Advance_CSS/Icons/car.png" style="width: 35px;" /></span>
                                                    <span data-toggle="tooltip" data-original-title="Swimming Pool">
                                                        <img src="../Advance_CSS/Icons/food.png" style="width: 35px;" /></span>
                                                    <span data-toggle="tooltip" data-original-title="Business Facilities">
                                                        <img src="../Advance_CSS/Icons/sight.png" style="width: 35px;" /></span>

                                                    <%-- <span class="cf border rounded-pill text-1 px-2">Couple Friendly</span>--%>
                                                </p>

                                            </div>
                                            <div class="card-footer bg-transparent d-flex align-items-center">
                                                <i class="icofont-tag icofont-2x"></i>

                                               <%#Convert.ToBoolean(Eval("pkg_offerPrice")) == true ? "<div class='d-block text-3 text-black-50 mr-2 mr-lg-3'><del class='d-block'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</del></div><div class='text-dark font-weight-500 mr-2 mr-lg-3' style='color: red !important;font-size: 1.1rem !important;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_Offer_price_value")), 2)+"</div>" : "<div class='text-dark  font-weight-500 mr-2 mr-lg-3' style='color: red !important;font-size: 1.1rem !important;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</div>" %>

                                                
                                                
<%--                                                <div class="text-success text-3">30% Off!</div>--%>
                                                <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>" class="btn btn-sm btn-primary ml-auto">View</a>
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:Repeater>

                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>


    </form>
        <div class="theme-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="theme-copyright-text">
                        Copyright &copy; 2021
             
                        <a href="#">Tripforo</a>. All rights reserved.
           
                    </p>
                </div>
                <div class="col-md-6">
                    <ul class="theme-copyright-social">
                        <li>
                            <a class="fa fa-facebook" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-google" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-twitter" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-youtube-play" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-instagram" href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

   

