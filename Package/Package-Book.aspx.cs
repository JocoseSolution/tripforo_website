﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Drawing;
using System.IO;
using System.Web.Services;
public partial class Package_Package_Book : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString());
    public long pkgid;
    StringBuilder hotelcategory = new StringBuilder();
    string EmlBodyToClient = string.Empty;
    string EmlBodyToAdmin = string.Empty;
    public string ImageList = string.Empty;
    public string ImageList1 = string.Empty;
    public bool IsImgPNShow = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        Int64 pkgid = Convert.ToInt64(Request.QueryString["Pkg_Id"]);
        if (!Page.IsPostBack)
        {
            ShowPkgDetails(pkgid);
            LastMinutesDeal();
            ShowPkgImageDetails(pkgid);
        }
    }

    public void ShowPkgDetails(long packgId)
    {

        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetSelectedPackageDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pkgid", packgId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PkgDetails.DataSource = dt;
            PkgDetails.DataBind();
            con.Close();
        }


        catch (Exception ex)
        {
            con.Close();

            clsErrorLog.LogInfo(ex);
        }
    }

    //protected void Submit_Click(object sender, EventArgs e)
    //{
    //    con.Open();
    //    String sp = "InsertPackageBooking";

    //    SqlCommand cmd = new SqlCommand(sp, con);
    //    cmd.CommandType = CommandType.StoredProcedure;
    //    cmd.Parameters.AddWithValue("@pkgid", pkgid);
    //    cmd.Parameters.AddWithValue("@fullname", txtFullName.Text);
    //    cmd.Parameters.AddWithValue("@email", txtEmailId.Text);
    //    cmd.Parameters.AddWithValue("@mobile", txtMobNo.Text);
    //    cmd.Parameters.AddWithValue("@address", txtAddress.Text);
    //    cmd.Parameters.AddWithValue("@notes", txtAddNotes.Text);

    //    cmd.Parameters.AddWithValue("@bookingdate", DateTime.Now);

    //    cmd.ExecuteNonQuery();
    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Thank You for Booking')", true);



    //    con.Close();

    //}

    protected void LastMinutesDeal()
    {
        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetLastMinutesPkgDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt != null)
            {
                LastMinutesPkgDetails.DataSource = dt;
                LastMinutesPkgDetails.DataBind();
                con.Close();
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            con.Close();

            clsErrorLog.LogInfo(ex);
        }
    }


    public void GetHotelCategory()
    {
        int i = 0;
        try
        {

            con.Open();
            String sp = "";

            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader ar = cmd.ExecuteReader();
            if (ar.HasRows)
            {

                while (ar.Read())
                {
                    i = Convert.ToInt32(ar["Hotel_Category"]);
                }
                for (int n = 0; n < i; n++)
                {
                    hotelcategory.Append("<i class='icofont - star text - warning'></i>");
                }
                ar.Close();
                ar.Dispose();

            }

            con.Close();
        }
        catch (Exception ex)
        {

            clsErrorLog.LogInfo(ex);
        }



    }

    [WebMethod]
    public static string SendEmailFromPortal(string name, string mobile, string email, string address, string notes,string pkgid)
    {
        string rutStr = string.Empty;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString.ToString());
        try
        {
            con.Open();
            String sp = "InsertPackageBooking";
            SqlCommand cmd = new SqlCommand(sp, con);
            //string pkgid = HttpContext.Current.Request.QueryString["Pkg_Id"];
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pkgid", pkgid);
            cmd.Parameters.AddWithValue("@fullname", name);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@mobile", mobile);
            cmd.Parameters.AddWithValue("@address", address);
            cmd.Parameters.AddWithValue("@notes", notes);

            cmd.Parameters.AddWithValue("@bookingdate", DateTime.Now);
            // string body = "<p>"+ name + "</p>"+ "<p>" + mobile + "</p>"+ "<p>" + address + "</p>"+ "<p>" + notes + "</p>";
            string ToClientBody = EmlTemp1();
            string ToClienAdmintBody = EmlTemp2();
            ToClienAdmintBody = ToClienAdmintBody.Replace("#Name", name).Replace("#email", email).Replace("#mob",mobile).Replace("#pkgid", pkgid).Replace("#address", address).Replace("#notes", notes);
            string subject1 ="Thank you "+ name;
            string subject2 = "New Package Booking " + name;
            if (cmd.ExecuteNonQuery() > 0)
            {
                rutStr = "success";
              int n= SendMail1(email, "Info@faremonster.in", "smtp.gmail.com", ToClientBody, subject1);
              int i= SendMail1("Info@faremonster.in", "Info@faremonster.in", "smtp.gmail.com", ToClienAdmintBody, subject2);
                if(n==1)
                {

                }
            }
            con.Close();

        }
        catch (Exception ex)
        {
            con.Close();
            rutStr = "fail";
        }
        return rutStr;
    }

    public static int SendMail1(string toEMail, string from, string smtpClient, string body, string subject)
    {
        System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
        System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
        msgMail.To.Clear();
        msgMail.To.Add(new System.Net.Mail.MailAddress(toEMail));
        msgMail.From = new System.Net.Mail.MailAddress(from);

        msgMail.Subject = subject;
        msgMail.IsBodyHtml = true;
        msgMail.Body = body;


        try
        {
            objMail.Credentials = new System.Net.NetworkCredential("Info@faremonster.in", "Sonusid82@");
            objMail.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            objMail.EnableSsl = true;

            objMail.Port = 25;
            objMail.Host = smtpClient;
            // objMail.Timeout = 10000
            objMail.Send(msgMail);
            return 1;
        }
        catch (Exception ex)
        {
            // clsErrorLog.LogInfo(ex);
            return 0;
        }
    }

    public static string EmlTemp1()
    {
        string EmlBodyToClient = string.Empty;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString.ToString());

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM PkgBookEmlTemp", con);
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                EmlBodyToClient = dr["ToClient"].ToString();
            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }


        return EmlBodyToClient;
    }
    public static string EmlTemp2()
    {
        string EmlBodyToAdmin = string.Empty;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString.ToString());

        try
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM PkgBookEmlTemp", con);
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                EmlBodyToAdmin = dr["ToAdmin"].ToString();
            }
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }


        return EmlBodyToAdmin;
    }

    public void ShowPkgImageDetails(long packgId)
    {

        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetPkg_Imagelist", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pkg_id", packgId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            con.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows.Count > 1)
                {
                    IsImgPNShow = true;
                }

                for (int i = 0; i <= dt.Rows.Count; i++)
                {
                    ImageList += "<div class='mySlides'> <img src='/HolidaysImage/" + dt.Rows[i]["pkg_Image"].ToString() + "' style='width: 100%; max-height:500px;'> </div>";
                }
            }
        }


        catch (Exception ex)
        {
            con.Close();

            clsErrorLog.LogInfo(ex);
        }
    }
}