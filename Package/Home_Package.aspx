﻿<%@ Page Title="" Language="VB"  AutoEventWireup="false" CodeFile="Home_Package.aspx.vb" Inherits="Package_Home_Package" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Package Home</title>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"/>
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<%--    <link href="../Advance_CSS/css/bootstrap.css" rel="stylesheet" />--%>
    <link href="../Advance_CSS/css/styles.css" rel="stylesheet" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style type="text/css">
        .primary-menu ul.navbar-nav > li > a {
            height: 70px;
            padding-left: 0.85em;
            padding-right: 0.85em;
            color: #fff;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            position: relative;
        }

        .img-fluid {
            max-width: 100%;
            height: 72px;
        }

        .carousel-inner {
            position: relative;
            width: 100%;
            overflow: hidden;
        }

            .carousel-inner:hover {
                position: relative;
                width: 100%;
                overflow: visible;
            }

        .photo-container:hover {
            /* transform: scale(1.5);
            overflow:visible;
            z-index:99999;*/
            box-shadow: 0px 0px 150px #000000;
            z-index: 2;
            -webkit-transition: all 200ms ease-in;
            -webkit-transform: scale(1.5);
            -ms-transition: all 200ms ease-in;
            -ms-transform: scale(1.5);
            -moz-transition: all 200ms ease-in;
            -moz-transform: scale(1.5);
            transition: all 200ms ease-in;
            transform: scale(1.5);
            overflow: visible !important;
        }


        .hero-wrap .hero-bg {
            z-index: 0;
            background-attachment: inherit !important;
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            transition: background-image 300ms ease-in 200ms;
        }

        .bg-light {
            background: url(https://pngimage.net/wp-content/uploads/2018/06/schnee-png-3.png) !important;
            background-color: #fff !important;
        }


        #header {
            background: #efe4e461 !important;
            margin: 0 auto;
            /*padding: .5em 0 4.5em 0;*/
            position: absolute;
            z-index: 9;
            width: 100%;
        }


        /* .primary-menu ul.navbar-nav > li > a {
            height: 70px;
            padding-left: 0.85em;
            padding-right: 0.85em;
            color: #000;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            position: relative;
            font-size: 20px;
            font-weight: bold;
        }*/

        .heading-bg {
            margin: 0;
            padding: 0.7em 0 0.3em 0;
            position: relative;
            background: url(../images/bg/blue.png) no-repeat center top !important;
            font-size: 60px;
            font-family: 'Bebas Neue';
            text-align: center;
            font-weight: 500;
            line-height: 20px;
        }

        .card-img-top {
            width: 100%;
            border-top-left-radius: calc(.25rem - 1px);
            border-top-right-radius: calc(.25rem - 1px);
            height: 200px;
        }

        div.package-ribbon-wrapper {
            position: absolute;
            right: -8px;
            bottom: 155px;
        }

        .package-type.last-minute {
            background-color: #ff634d;
            color: #fff;
        }

        div.package-type {
            padding: 9px 12px;
            font-size: 14px;
            line-height: 1;
        }

            div.package-type span.head {
                font-size: 12px;
                display: block;
                line-height: 1;
                margin-bottom: 4px;
            }

            div.package-type span.discount-text {
                font-size: 18px;
                display: block;
                line-height: 1;
            }

        div.package-type-gimmick {
            width: 0;
            height: 0;
            float: right;
            border-width: 8px 8px 0 0;
            border-style: solid;
            border-bottom-color: transparent;
            border-right-color: transparent;
            border-left-color: transparent;
        }

        .package-type-gimmick {
            border-top-color: #000;
        }

        div.card .package-title-wrapper {
            position: absolute;
            bottom: 60px;
            left: 10px;
            right: 10px;
            padding: 8px 10px;
        }

        div.card .package-title-overlay {
            background: #000000ad;
            opacity: .8;
            filter: alpha(opacity=80);
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            top: 0;
            border-radius: 5px;
        }

        div.card .package-title {
            font-size: 18px;
            margin-top: -1px;
            margin-bottom: 0;
            float: left;
            position: relative;
            line-height: 20px;
        }

            div.card .package-title a {
                color: #fff;
            }

        div.card .package-info {
            font-size: 14px;
            line-height: 20px;
            font-weight: 700;
            float: right;
            position: relative;
        }


        div.card .package-price {
            color: #ff0303;
            font-weight: normal;
        }

        div.card .package-date {
            margin-bottom: 8px;
            font-size: 12px;
            margin-top: 8px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-weight: 700;
            text-align: center;
        }

            div.card .package-date i {
                margin-right: 7px;
                font-size: 15px;
            }
    </style>
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        :root {
            --grid-column-gap: 20px;
        }



        .carouselPre {
            grid-auto-flow: column;
            display: grid;
            grid-auto-columns: calc((98% - 3 * var(--grid-column-gap))/ 5);
            grid-column-gap: var(--grid-column-gap);
            /*scroll-snap-type: x mandatory;*/
            /*overflow-x: scroll;
            overflow-y: hidden;*/
            list-style: none;
            /*scroll-behavior: smooth;*/
        }

        .carouselPre1 {
            grid-auto-flow: column;
            display: grid;
            grid-auto-columns: calc((98% - 3 * var(--grid-column-gap))/ 5);
            grid-column-gap: var(--grid-column-gap);
            /*scroll-snap-type: x mandatory;*/
            /*overflow-x: scroll;
            overflow-y: hidden;*/
            list-style: none;
            /*scroll-behavior: smooth;*/
        }



        #title {
            margin: 10px 10px 10px 0;
            font-size: 34px;
            font-weight: 600;
            text-align: center;
        }

        @media (max-width:500px) {
            #title {
                margin: 10px 147px 13px 0;
                font-size: 22px;
                font-weight: 600;
                text-align: center;
            }
        }

        .topT {
            padding: 10px 0;
            font-weight: 600;
            font-size: 20px;
        }

        .itemsC .textDet {
            position: absolute;
            bottom: 0;
            height: 60px;
            width: 100%;
            display: flex;
        }

        .contents {
            color: white;
        }

        .itemsC .contents p {
            -webkit-line-clamp: 3;
            text-align: center;
            display: -webkit-box;
            overflow: hidden;
            -webkit-box-orient: vertical;
            font-size: 12px;
            line-height: 1.25;
            font-weight: 400;
            width: 90%;
            margin: auto;
        }

        .leftTxtDet {
            position: absolute;
            text-align: center;
            width: 100%;
            top: 33px;
        }

        .itemsC {
            position: relative;
        }





        .scrolly {
            overflow: hidden;
            position: relative;
        }

        .carouselPre li {
            scroll-snap-align: start;
            width: 100%;
            height: 100%;
            list-style: none;
            border-radius: 20px;
            overflow: hidden;
            height: 400px;
            margin: 0 2px;
        }

        .carouselPre1 li {
            scroll-snap-align: start;
            width: 100%;
            height: 100%;
            list-style: none;
            border-radius: 20px;
            overflow: hidden;
            height: 400px;
            margin: 0 2px;
        }

        .carouselPre li .bgImg {
            width: 100%;
            height: 100%;
        }

        .carouselPre1 li .bgImg {
            width: 100%;
            height: 100%;
        }

        .carouselPre li img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .carouselPre1 li img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }



        #left, #right {
            background-color: rgba(255, 255, 255, 0.9);
            outline: none;
            border: transparent;
            border-radius: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 3rem;
            height: 3rem;
            position: absolute;
            top: 50%;
            transform: translate(0,-60%);
            cursor: pointer;
            z-index: 2;
        }

        #left {
            left: 0;
        }

        #right {
            right: 0;
        }

        @media only screen and (max-width: 890px) {
            .carouselPre {
                grid-auto-columns: calc((100% - 2 * var(--grid-column-gap))/ 3);
                height: 23rem;
            }

            .carouselPre1 {
                grid-auto-columns: calc((100% - 2 * var(--grid-column-gap))/ 3);
                height: 23rem;
            }
        }

        @media only screen and (max-width: 660px) {
            .carouselPre {
                grid-auto-columns: calc((100% - 1 * var(--grid-column-gap))/ 2);
                height: 25rem;
            }

            .carouselPre1 {
                grid-auto-columns: calc((100% - 1 * var(--grid-column-gap))/ 2);
                height: 25rem;
            }
        }

        @media only screen and (max-width: 440px) {
            .carouselPre {
                grid-auto-columns: 100%;
                height: 25rem;
            }

            .carouselPre1 {
                grid-auto-columns: 100%;
                height: 25rem;
            }
        }


        ::-webkit-scrollbar {
            width: 15px;
            height: 15px;
            border-left: #ededed solid 1px;
            background-color: #fcfcfc;
            border-top: #ededed solid 1px;
        }

        ::-webkit-scrollbar-thumb:hover {
            cursor: pointer;
            background: #c7c7c7;
            width: 15px;
            background-clip: content-box;
            border: 4px solid transparent;
            border-radius: 10px;
        }

        ::-webkit-scrollbar-button {
            display: none;
        }

        ::-webkit-scrollbar-thumb {
            background: #dbdbdb;
            background-clip: content-box;
            border: 4px solid transparent;
            border-radius: 10px;
        }


        .bg {
            margin: 0;
            padding: 0;
            position: absolute;
            z-index: 9;
            background: url(../images/bg/package-brush-top.png) no-repeat center bottom;
            height: 97px;
            display: block;
            width: 100%;
        }

        .bg2 {
            margin: 0;
            padding: 0;
            position: absolute;
            z-index: 9;
            background: url(../images/bg/package-brush-bot-1.png) no-repeat center bottom;
            height: 97px;
            display: block;
            width: 100%;
            margin-top: -112px;
        }

        .bg-top {
            margin: 0;
            padding: 0;
            /*position: absolute;  */
            z-index: 9;
            background: url(../images/bg/top-bg.png) no-repeat center bottom;
            height: 97px;
            display: block;
            width: 100%;
            margin-top: -114px;
            margin-top: 198px;
        }



        .itemsC .back {
            background: #444;
            color: #fff;
            text-align: center;
            padding: 4em 1em 6em 1em;
            /*font-family: 'Bebas Neue';*/
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            height: inherit !important;
        }



        /*.flip-card {
  background-color: transparent;
  width: 300px;
  height: 300px;
  perspective: 1000px;
}*/

        .flip-card-inner {
            position: relative;
            width: 100%;
            height: 100%;
            text-align: center;
            transition: transform 0.6s;
            transform-style: preserve-3d;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }

        .itemsC:focus {
            outline: 0;
        }

            .itemsC:hover .flip-card-inner,
            .itemsC:focus .flip-card-inner {
                transform: rotateY(180deg);
            }

        .flip-card-front,
        .flip-card-back {
            position: absolute;
            width: 100%;
            height: 100%;
            backface-visibility: hidden;
        }

        .flip-card-front {
            background: linear-gradient(to left, #4364f7, #6fb1fc);
            color: black;
            z-index: 2;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .flip-card-back {
            transform: rotateY(180deg);
            background: #003351;
            color: #fff;
            text-align: center;
            padding: 4em 1em 6em 1em;
            /* font-family: 'Bebas Neue'; */
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            height: inherit !important;
        }

        h3 {
            font-size: 20px;
            font-family: Verdana, sans-serif;
            font-weight: bold;
            color: #fff;
        }

        .pkg-fare {
            padding: 15% 5% 35% 5%;
            margin: 0;
            display: block;
            position: absolute;
            overflow: hidden;
            width: 100%;
            left: 0;
            text-align: center;
            right: 0;
            bottom: 0;
            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.49) 29%, rgba(0, 0, 0, 0.7) 51%, rgba(0, 0, 0, 0.94) 99%, rgba(0, 0, 0, 0.94) 100%);
            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.49) 29%, rgba(0, 0, 0, 0.7) 51%, rgba(0, 0, 0, 0.94) 99%, rgba(0, 0, 0, 0.94) 100%);
            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.49) 29%, rgba(0, 0, 0, 0.7) 51%, rgba(0, 0, 0, 0.94) 99%, rgba(0, 0, 0, 0.94) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#f0000000', GradientType=0 );
        }

            .pkg-fare h4 a {
                color: #fff !important;
            }

            .pkg-fare h6 {
                color: #fff !important;
            }
    </style>

    <style type="text/css">
        .testim-box {
            margin: 0;
            padding: 0;
            position: relative;
        }

        #banner-cont img {
            margin: 0;
            padding: 0;
            width: 100%;
            border: 1px solid #fff;
        }

        .testim-box img {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 291px;
        }

        .testim-box-brush {
            margin: 0;
            padding: 0;
            position: absolute;
            z-index: 9;
            width: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            background-size: cover;
            background-position: center center;
        }

        .testim-box-bot {
            margin: 0;
            padding: 1.1em 1.5em 2.5em 1.5em;
            position: relative;
            background: #FFF;
            box-shadow: 0 3px 15px #999;
            z-index: 999;
            width: 90%;
            margin-top: -70px;
            left: 15px;
            right: 15px;
            margin-bottom: -10px;
            height: auto;
            border-radius: 25px;
        }

            .testim-box-bot p {
                margin: 0 0 1em 0;
                padding: 0;
                font-size: 14px;
                line-height: 24px;
                color: #1a2228;
                text-align: justify;
            }
    </style>

    <style type="text/css">
        .feature-info-style-02 {
            background: #ffffff;
            padding: 35px;
            overflow: hidden;
            position: relative;
            -webkit-box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
            box-shadow: 5px 5px 24px 0px rgb(2 45 98 / 10%);
            border-radius: 5px;
            cursor: pointer;
        }

        .bg-dark {
            background-color: #022d62 !important;
        }

        .feature-info-style-02 .feature-info-icon {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            margin-bottom: 15px;
            position: inherit;
            top: 0;
        }

        .text-white {
            color: #fff !important;
            font-size: 35px;
        }

        .ms-4 {
            margin-left: 1.5rem !important;
        }

        .mb-0 {
            margin-bottom: 0 !important;
        }

        .bg-primary {
            background-color: #d0b054 !important;
        }
    </style>

    <style type="text/css">
        .carousel-inner {
            width: 100%;
            display: inline-block;
            position: relative;
        }

        .carousel-inner {
            padding-top: 42.25%;
            display: block;
            content: "";
        }


        @media (max-width:500px) {
            .carousel-inner {
                padding-top: 51.25%;
                display: block;
                content: "";
                height: 647px !important;
            }
            .testo
            {
                margin: 9px 5px 13px 0 !important;
            }
            p {
                text-align: justify !important;
                font-size: 12px !important;
            }
        }

        .carousel-item {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            background: skyblue;
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .caption {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translateX(-50%) translateY(-50%);
            width: 60%;
            z-index: 9;
            margin-top: 20px;
            text-align: center;
        }

            .caption h1 {
                color: #fff;
                font-size: 52px;
                font-weight: 700;
                margin-bottom: 23px;
                font-family: "Berkshire Swash", cursive;
            }

            .caption h2 {
                color: rgba(255,255,255,.75);
                font-size: 26px;
                font-weight: 300;
            }

        a.big-button {
            color: #fff;
            font-size: 19px;
            font-weight: 700;
            text-transform: uppercase;
            background: #eb7a00;
            background: rgba(255, 0, 0, 0.75);
            padding: 28px 35px;
            border-radius: 3px;
            margin-top: 80px;
            margin-bottom: 0;
            display: inline-block;
        }

            a.big-button:hover {
                text-decoration: none;
                background: rgba(255, 0, 0, 0.9);
            }

        a.view-demo {
            color: #fff;
            font-size: 21px;
            font-weight: 700;
            display: inline-block;
            margin-top: 35px;
        }

            a.view-demo:hover {
                text-decoration: none;
                color: #333;
            }

        .carousel-indicators .active {
            background: #fff;
        }

        .carousel-indicators li {
            background: rgba(255, 255, 255, 0.4);
            border-top: 20px solid;
            z-index: 15;
        }
    </style>

    <style type="text/css">
        @import url("https://fonts.googleapis.com/css?family=Roboto:700");

        @keyframes showTopText {
            0% {
                transform: translate3d(0, 100%, 0);
            }

            40%, 60% {
                transform: translate3d(0, 50%, 0);
            }

            100% {
                transform: translate3d(0, 0, 0);
            }
        }

        @keyframes showBottomText {
            0% {
                transform: translate3d(0, -100%, 0);
            }

            100% {
                transform: translate3d(0, 0, 0);
            }
        }

        .animated-title {
            color: #eee;
            font-family: Roboto, Arial, sans-serif;
            height: 90vmin;
            left: 50%;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%);
            width: 90vmin;
        }

            .animated-title > div {
                height: 50%;
                overflow: hidden;
                position: absolute;
                width: 100%;
            }

                .animated-title > div div {
                    font-size: 12vmin;
                    padding: 2vmin 0;
                    position: absolute;
                }

                    .animated-title > div div span {
                        display: block;
                    }

                .animated-title > div.text-top {
                    border-bottom: 1vmin solid #f88;
                    top: 0;
                }

                    .animated-title > div.text-top div {
                        animation: showTopText 1s;
                        animation-delay: 0.5s;
                        animation-fill-mode: forwards;
                        bottom: 0;
                        transform: translate(0, 100%);
                    }

                        .animated-title > div.text-top div span:first-child {
                            color: #fff;
                        }

                .animated-title > div.text-bottom {
                    bottom: 0;
                }

                    .animated-title > div.text-bottom div {
                        animation: showBottomText 0.5s;
                        animation-delay: 1.75s;
                        animation-fill-mode: forwards;
                        top: 0;
                        transform: translate(0, -100%);
                    }
    </style>

    <style type="text/css">
        .h4, h4 {
            font-size: 1.1rem !important;
        }
    </style>
    <style type="text/css">
        @media (max-width: 991px) {
            /* Mobile Menu Button */

            #carousel .carousel slide {
                height: 200px;
            }

            .navbar-toggler {
                width: 25px;
                height: 30px;
                padding: 10px;
                margin: 18px 10px;
                position: relative;
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
                -webkit-transition: 0.5s ease-in-out;
                transition: 0.5s ease-in-out;
                cursor: pointer;
                display: block;
            }

                .navbar-toggler span {
                    display: block;
                    position: absolute;
                    height: 2px;
                    width: 100%;
                    background: #3c3636;
                    border-radius: 2px;
                    opacity: 1;
                    left: 0;
                    -webkit-transform: rotate(0deg);
                    transform: rotate(0deg);
                    -webkit-transition: 0.25s ease-in-out;
                    transition: 0.25s ease-in-out;
                }

                    .navbar-toggler span:nth-child(1) {
                        top: 6px;
                        -webkit-transform-origin: left center;
                        -moz-transform-origin: left center;
                        -o-transform-origin: left center;
                        transform-origin: left center;
                    }

                    .navbar-toggler span:nth-child(2) {
                        top: 12px;
                        -webkit-transform-origin: left center;
                        -moz-transform-origin: left center;
                        -o-transform-origin: left center;
                        transform-origin: left center;
                    }

                    .navbar-toggler span:nth-child(3) {
                        top: 18px;
                        -webkit-transform-origin: left center;
                        -moz-transform-origin: left center;
                        -o-transform-origin: left center;
                        transform-origin: left center;
                    }

                .navbar-toggler.open span:nth-child(1) {
                    top: 5px;
                    left: 4px;
                    -webkit-transform: rotate(45deg);
                    transform: rotate(45deg);
                }

                .navbar-toggler.open span:nth-child(2) {
                    width: 0%;
                    opacity: 0;
                }

                .navbar-toggler.open span:nth-child(3) {
                    top: 21px;
                    left: 4px;
                    -webkit-transform: rotate(-45deg);
                    transform: rotate(-45deg);
                }

            #header .primary-menu {
                position: absolute;
                top: 99%;
                right: 0;
                left: 0;
                background: transparent;
                margin-top: 0px;
                z-index: 1000;
            }

                #header .primary-menu:before {
                    content: '';
                    display: block;
                    position: absolute;
                    top: 0;
                    left: 50%;
                    width: 100vw;
                    height: 100%;
                    background: #fff;
                    z-index: -1;
                    -webkit-transform: translateX(-50%);
                    transform: translateX(-50%);
                    -webkit-box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
                    box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
                }

                #header .primary-menu > div {
                    overflow: hidden;
                    overflow-y: auto;
                    max-height: 65vh;
                    margin: 18px 0;
                    margin-left: 10px !important;
                }

            .primary-menu ul.navbar-nav li {
                display: block;
                border-bottom: 1px solid #eee;
                margin: 0;
                padding: 0;
            }

                .primary-menu ul.navbar-nav li:last-child {
                    border: none;
                }

                .primary-menu ul.navbar-nav li.dropdown > .dropdown-toggle > .arrow.open:after {
                    -webkit-transform: translate(-50%, -50%) rotate(-45deg);
                    transform: translate(-50%, -50%) rotate(-45deg);
                    -webkit-transition: all 0.2s ease;
                    transition: all 0.2s ease;
                }

            .primary-menu ul.navbar-nav > li > a {
                height: auto;
                padding: 8px 0;
                position: relative;
                color: #0e0e0e !important;
            }

            .primary-menu ul.navbar-nav > li > a:hover {
               
                color: #fd7e14 !important;
            }

            .primary-menu ul.navbar-nav > li.dropdown .dropdown-menu li > a:not(.btn) {
                padding: 8px 0;
                position: relative;
            }

            .primary-menu ul.navbar-nav > li.dropdown:hover > a:after {
                content: none;
            }

            .primary-menu ul.navbar-nav > li.dropdown .dropdown-toggle .arrow:after {
                -webkit-transform: translate(-50%, -50%) rotate(134deg);
                transform: translate(-50%, -50%) rotate(134deg);
                -webkit-transition: all 0.2s ease;
                transition: all 0.2s ease;
            }

            .primary-menu ul.navbar-nav > li.dropdown .dropdown-menu {
                margin: 0;
                -webkit-box-shadow: none;
                box-shadow: none;
                border: none;
                padding: 0px 0px 0px 15px;
            }

                .primary-menu ul.navbar-nav > li.dropdown .dropdown-menu .dropdown-menu {
                    margin: 0;
                }

            .primary-menu ul.navbar-nav > li.dropdown-mega .dropdown-mega-content > .row > div {
                padding: 0px 15px;
            }

            .primary-menu ul.navbar-nav > li.dropdown-mega .sub-title {
                margin-top: 10px;
                display: block;
                padding: 0;
            }

            .primary-menu ul.navbar-nav > li.login-signup > a:before {
                content: none;
            }
        }

        .navbar-theme-border .navbar-nav {
    margin-bottom: -1px !important;
}
@media (min-width: 768px)
{

.navbar-nav {
    float: right;
    margin: 0;
}
}
.navbar-nav {
    margin: 7.5px -15px;
}
.nav {
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}
ul, ol {
    margin-top: 0;
    margin-bottom: 10px;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
               <nav class="navbar navbar-default navbar-inverse navbar-theme navbar-theme-abs navbar-theme-transparent navbar-theme-border" id="main-nav">
        <div class="container">
            <div class="navbar-inner nav">
                <div class="navbar-header">
                    
                    <a class="navbar-brand" href="/Login.aspx">
                        <img src="../Advance_CSS/Icons/logo(ft).png" alt="tripforo" title="Tripforo" />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main">
                    <ul class="nav navbar-nav">
                       <li>
                            <a href="Package/Home_Package.aspx">Package</a>

                        </li>
                        <li>
                            <a href="about-us.aspx">About</a>

                        </li>
                        <li>
                            <a href="contact-us.aspx">Contact</a>

                        </li>                       

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                         
                       

                       
                    </ul>
                </div>
            </div>
        </div>
    </nav>

        
  
    <div id="content">



        <div id="carousel" class="carousel slide" data-ride="carousel" style="height: 650px;">
            <%--    <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>--%>

            <div class="carousel-inner" role="listbox">

                <div class="carousel-item active" style="background-image: url('../Advance_CSS/Images/slide1.jpg'); background-size: cover; background-position: center;">
                    <%--<div class="caption">
                        <h1>Experience Variety</h1>
                        <h2>Make it easy for you to do whatever this thing does.</h2>
                    </div>--%>

                    <div class="animated-title">
                        <div class="text-top">
                            <div>
                                <span style="margin-bottom: 20px; font-size: 35px;">Experience Variety</span>

                            </div>
                        </div>
                        <div class="text-bottom">
                            <div style="font-size: 18px;">Make it easy for you to do whatever this thing does.</div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" style="background-image: url('../Advance_CSS/Images/slide2.jpg'); background-size: cover; background-position: center;">
                    <%--  <div class="caption">
                        <h1>Eternally Yours</h1>
                        <h2>Else it easy for you to do whatever this thing does.</h2>

                       
                    </div>--%>

                    <div class="animated-title">
                        <div class="text-top">
                            <div>
                                <span style="margin-bottom: 20px; font-size: 35px;">Eternally Yours</span>

                            </div>
                        </div>
                        <div class="text-bottom">
                            <div style="font-size: 18px;">Else it easy for you to do whatever this thing does.</div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item" style="background-image: url('../Advance_CSS/Images/slide3.jpg'); background-size: cover; background-position: center;">
                    <%--<div class="caption">
                        <h1>Different World</h1>
                        <h2>Make it easy for you to do whatever this thing does.</h2>
                    </div>--%>

                    <div class="animated-title">
                        <div class="text-top">
                            <div>
                                <span style="margin-bottom: 20px; font-size: 35px;">Different World</span>

                            </div>
                        </div>
                        <div class="text-bottom">
                            <div style="font-size: 18px;">Make it easy for you to do whatever this thing does.</div>
                        </div>
                    </div>
                </div>

            </div>

            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>




        <section class="gallery" style="display: none;">
            <div class="flexbox-container">
                <div class="flexbox-container">
                    <div class="half left top img-container">
                        <div class="carousel slide carousel-multi-item" data-ride="carousel" id="multi-item-example1">
                            <div class="carousel-inner" role="listbox" style="width: 100% !important;">

                                <div class="carousel-item active">
                                    <asp:Repeater ID="RightPkg" runat="server">
                                        <ItemTemplate>
                                            <div class="half left top img-container">

                                                <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                                    <div class="wow fadeInLeft" data-wow-delay=".35s" style="background: #fff; width: 100%; margin-left: auto; margin-right: auto; padding: 8px; box-shadow: rgb(0 0 0 / 50%) 0px 0px 20px 0px;">

                                                        <img alt="Card image cap" class="pic-thumbnail" src="http://admin.tripforo.com/HolidaysImage/<%# Eval("pkg_Image") %>">


                                                        <span><%# Eval("pkg_Title") %></span>
                                                    </div>
                                                </a>

                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                </div>
                                <div class="carousel-item">
                                    <asp:Repeater ID="LeftPkg" runat="server">
                                        <ItemTemplate>


                                            <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                                <div class="wow fadeInLeft" data-wow-delay=".35s" style="background: #fff; width: 100%; margin-left: auto; margin-right: auto; padding: 8px; box-shadow: rgb(0 0 0 / 50%) 0px 0px 20px 0px;">

                                                    <img alt="Card image cap" class="pic-thumbnail" src="http://admin.tripforo.com/HolidaysImage/<%# Eval("pkg_Image") %>">


                                                    <span><%# Eval("pkg_theme") %></span>
                                                </div>
                                            </a>


                                        </ItemTemplate>
                                    </asp:Repeater>

                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="half right bottom text-container">
                        <h2 style="text-shadow: 1px 1px 5px rgb(0 0 0 / 50%);">See Our Popular Packages
                        </h2>
                        <p style="font-family: 'Caveat', cursive !important; color: #000; font-size: 26px;">
                            “Take only memories, leave only footprints”
                        </p>

                        <div class="container mt-3 mb-5">
                            <div class="row">
                                <div class="col">
                                    <!--Carousel Wrapper-->
                                    <div class="carousel slide carousel-multi-item" data-ride="carousel" id="multi-item-example">

                                        <div class="controls-top float-right">
                                            <span class="prev" data-slide="prev" href="#multi-item-example" style="position: absolute; left: -3%; top: 50%; color: red; cursor: pointer;"><i class="fa fa-chevron-left"></i></span>
                                            <span class="next" data-slide="next" href="#multi-item-example" style="position: absolute; right: -3%; top: 50%; color: red; cursor: pointer;"><i class="fa fa-chevron-right"></i></span>
                                        </div>

                                        <div class="carousel-inner" role="listbox" style="width: 100% !important;">

                                            <div class="carousel-item active">
                                                <div class="row">
                                                    <asp:Repeater ID="Repeater1" runat="server">
                                                        <ItemTemplate>

                                                            <div class="col-md-2">
                                                                <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                                                    <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                                                        <div class="pic-thumbnail">
                                                                            <img alt="Card image cap" class="img-fluid" src="http://b2b.faremonster.in/support/HolidaysImage/<%# Eval("pkg_Image") %>">
                                                                        </div>
                                                                        <span><%# Eval("pkg_Title") %></span>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </div>


                                            <div class="carousel-item">
                                                <div class="row">
                                                    <asp:Repeater ID="Repeater2" runat="server">
                                                        <ItemTemplate>

                                                            <div class="col-md-2">
                                                                <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %>">
                                                                    <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                                                        <div class="pic-thumbnail">
                                                                            <img alt="Card image cap" class="img-fluid" src="http://admin.tripforo.com/HolidaysImage/<%# Eval("pkg_Image") %>">
                                                                        </div>
                                                                        <span><%# Eval("pkg_Title") %></span>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                    <%--  <div class="col-md-2 clearfix d-none d-md-block">

                                                        <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                                            <div class="pic-thumbnail">
                                                                <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">
                                                            </div>
                                                            <span>Spain8</span>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-2 clearfix d-none d-md-block">

                                                        <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                                            <div class="pic-thumbnail">
                                                                <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">
                                                            </div>
                                                            <span>Spai9</span>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-2 clearfix d-none d-md-block">

                                                        <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                                            <div class="pic-thumbnail">
                                                                <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">
                                                            </div>
                                                            <span>Spain10</span>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-2 clearfix d-none d-md-block">

                                                        <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                                            <div class="pic-thumbnail">
                                                                <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">
                                                            </div>
                                                            <span>Spain11</span>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-2 clearfix d-none d-md-block">

                                                        <div class="photo-container wow fadeInRight" data-wow-delay=".65s">
                                                            <div class="pic-thumbnail">
                                                                <img alt="Card image cap" class="img-fluid" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg">
                                                            </div>
                                                            <span>Spain12</span>
                                                        </div>

                                                    </div>--%>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>


        </section>



        <section class="section bg-light">
            <div id="Visible_Domestic" runat="server" visible="false">
                <div class="">

                    <h1 id="title" style="display: ">DOMESTIC PACKAGES</h1>
                    <a href="Tour_Package.aspx?type=Domestic" class="btn btn-sm btn-primary ml-auto" style="float: right; margin-top: -46px; margin-right: 29px;">View All <i class="icofont-double-right"></i></a>

                    <div class="scrolly">
                        <h2 class="topT"></h2>
                        <%-- <button id="left"><span class="icofont-rounded-left"></span></button>
                    <button id="right"><span class="icofont-rounded-right"></span></button>--%>
                        <ul class="carouselPre">

                            <asp:Repeater ID="PackageDomestic" runat="server">
                                <ItemTemplate>
                                    <li class="itemsC" tabindex="0">


                                        <div class="flip-card-inner">


                                            <div class="flip-card-front">
                                                <div class="pkg-fare" style="backface-visibility: hidden;">
                                                    <h4 style="backface-visibility: hidden;"><a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>" style="backface-visibility: hidden;"><%# Eval("pkg_Title") %> </a></h4>
                                                    <div class="mt-1" style="backface-visibility: hidden;"></div>

                                                    <h6 style="backface-visibility: hidden;">Starting From  <span class="WebRupee" style="font-size: 17px; backface-visibility: hidden;"></span>₹ <%# Convert.ToInt32(Eval("pkg_price")) %></h6>
                                                </div>
                                                <div class="bgImg">
                                                   
                                                    <img src="/HolidaysImage/<%# Eval("pkg_Image") %>" alt="N/A" ;" srcset="" width="20" height="30" />

                                                </div>
                                            </div>

                                            <div class="flip-card-back">
                                                <div class="moreblog">
                                                    <p class="mb-2" style="text-align: center !important;">
                                                        <span class="text-white-50"><%# Eval("pkg_noofday") %> Days / <%# Eval("pkg_noofnight") %> Nights</span>
                                                    </p>
                                                    <p style="font-size: 14px; text-align: center !important;">
                                                        <%# Eval("pkg_theme") %>
                                                    </p>
                                                </div>
                                                <a href='Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>' class="btn btn-warning" type="button">More Detail</a>
                                            </div>
                                        </div>

                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>


                        </ul>
                    </div>

                    <div class="bg2"></div>
                </div>
            </div>


            <div id="Visible_International" class="" runat="server" visible="false">
                <div class="">
                    <h1 id="title">INTERNATIONAL PACKAGES</h1>

                    <a href="Tour_Package.aspx?type=International" class="btn btn-sm btn-primary ml-auto" style="float: right; margin-top: -46px; margin-right: 29px;">View All <i class="icofont-double-right"></i></a>

                    <div class="scrolly">
                        <h2 class="topT"></h2>
                        <%--<button id="left1"><span class="icofont-rounded-left"></span></button>
                    <button id="right1"><span class="icofont-rounded-right"></span></button>--%>
                        <ul class="carouselPre1">

                            <asp:Repeater ID="PkgInternational" runat="server">
                                <ItemTemplate>

                                    <li class="itemsC" tabindex="0">

                                        <div class="flip-card-inner">


                                            <div class="flip-card-front">
                                                <div class="pkg-fare" style="backface-visibility: hidden;">
                                                    <h4 style="backface-visibility: hidden;"><a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>" style="backface-visibility: hidden;"><%# Eval("pkg_Title") %> </a></h4>
                                                    <div class="mt-1" style="backface-visibility: hidden;"></div>

                                                    <h6 style="backface-visibility: hidden;">Starting From  <span class="WebRupee" style="font-size: 17px; backface-visibility: hidden;"></span>₹ <%# Convert.ToInt32(Eval("pkg_price")) %></h6>
                                                </div>
                                                <div class="bgImg">
                                                    <img src="/HolidaysImage/<%# Eval("pkg_Image") %>" alt="image" " srcset="" />
                                                </div>
                                            </div>

                                            <div class="flip-card-back">
                                                <div class="moreblog">
                                                    <p class="mb-2" style="text-align: center !important;">
                                                        <span class="text-white-50"><%# Eval("pkg_noofday") %> Days / <%# Eval("pkg_noofnight") %> Nights</span>
                                                    </p>
                                                    <p style="font-size: 14px; text-align: center !important;">
                                                        <%# Eval("pkg_Title") %>
                                                    </p>
                                                </div>
                                                <a href="Package-Book.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>" class="btn btn-warning" type="button">More Detail</a>
                                            </div>
                                        </div>

                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>


                        </ul>
                    </div>

                    <div class="bg2"></div>
                </div>
            </div>
            <div class="">
                <div style="text-align: center">
                    <a href="Tour_Package.aspx?type=All" class="btn btn-sm btn-primary ml-auto">View all Packages <i class="icofont-double-right"></i></a>

                </div>
            </div>





        </section>

        <script src="https://use.fontawesome.com/1744f3f671.js"></script>

    </div>



    <script type="text/javascript">
        // vars
        'use strict'
        var testim = document.getElementById("testim"),
            testimDots = Array.prototype.slice.call(document.getElementById("testim-dots").children),
            testimContent = Array.prototype.slice.call(document.getElementById("testim-content").children),
            testimLeftArrow = document.getElementById("left-arrow"),
            testimRightArrow = document.getElementById("right-arrow"),
            testimSpeed = 4500,
            currentSlide = 0,
            currentActive = 0,
            testimTimer,
            touchStartPos,
            touchEndPos,
            touchPosDiff,
            ignoreTouch = 30;
        ;

        window.onload = function () {

            // Testim Script
            function playSlide(slide) {
                for (var k = 0; k < testimDots.length; k++) {
                    testimContent[k].classList.remove("active");
                    testimContent[k].classList.remove("inactive");
                    testimDots[k].classList.remove("active");
                }

                if (slide < 0) {
                    slide = currentSlide = testimContent.length - 1;
                }

                if (slide > testimContent.length - 1) {
                    slide = currentSlide = 0;
                }

                if (currentActive != currentSlide) {
                    testimContent[currentActive].classList.add("inactive");
                }
                testimContent[slide].classList.add("active");
                testimDots[slide].classList.add("active");

                currentActive = currentSlide;

                clearTimeout(testimTimer);
                testimTimer = setTimeout(function () {
                    playSlide(currentSlide += 1);
                }, testimSpeed)
            }

            testimLeftArrow.addEventListener("click", function () {
                playSlide(currentSlide -= 1);
            })

            testimRightArrow.addEventListener("click", function () {
                playSlide(currentSlide += 1);
            })

            for (var l = 0; l < testimDots.length; l++) {
                testimDots[l].addEventListener("click", function () {
                    playSlide(currentSlide = testimDots.indexOf(this));
                })
            }

            playSlide(currentSlide);

            // keyboard shortcuts
            document.addEventListener("keyup", function (e) {
                switch (e.keyCode) {
                    case 37:
                        testimLeftArrow.click();
                        break;

                    case 39:
                        testimRightArrow.click();
                        break;

                    case 39:
                        testimRightArrow.click();
                        break;

                    default:
                        break;
                }
            })

            testim.addEventListener("touchstart", function (e) {
                touchStartPos = e.changedTouches[0].clientX;
            })

            testim.addEventListener("touchend", function (e) {
                touchEndPos = e.changedTouches[0].clientX;

                touchPosDiff = touchStartPos - touchEndPos;

                console.log(touchPosDiff);
                console.log(touchStartPos);
                console.log(touchEndPos);


                if (touchPosDiff > 0 + ignoreTouch) {
                    testimLeftArrow.click();
                } else if (touchPosDiff < 0 - ignoreTouch) {
                    testimRightArrow.click();
                } else {
                    return;
                }

            })
        }
    </script>

    <script type="text/javascript">
            (function () {

                // we need to set this here for now.
                var marquee;

                // first, let's grab the element we're going to move around
                var marquee_el = document.querySelector('.carouselPre');
                var children = marquee_el.querySelectorAll('.itemsC');

                function createMarquee() {

                    if ('animate' in marquee_el && typeof marquee_el.animate === 'function') {

                        if (typeof marquee !== 'undefined') marquee.cancel();

                        marquee_el.style.whiteSpace = 'nowrap';

                        var displacement = 0;

                        for (var j = 0; j < children.length; ++j) displacement += children[j].clientWidth;

                        displacement = (displacement - marquee_el.clientWidth) << 0;

                        marquee = marquee_el.animate([

                            { transform: 'matrix(1, 0.00, 0.00, 1, 0, 0)', offset: 0 },
                            { transform: 'matrix(1, 0.00, 0.00, 1,' + -displacement + ', 0)', offset: 1 }

                        ],
                            {

                                duration: children.length * 1e3,

                                easing: 'linear',

                                delay: 0,

                                // kind of crucial for a marquee...
                                iterations: Infinity,

                                // invert animation after completion, so it scrolls backwards */
                                direction: 'alternate',

                                fill: 'forwards'
                            });
                    }
                }


                if ('animate' in marquee_el && typeof marquee_el.animate === 'function') {

                    // okay, let's fire up the marquee!
                    createMarquee();

                    // now for the playing/pausing
                    marquee_el.addEventListener('mouseenter', pauseMarquee, false);
                    marquee_el.addEventListener('mouseleave', playMarquee, false);

                    // and resizing
                    window.addEventListener('resize', debounce(createMarquee), false);

                } else {

                    document.querySelector('h1').innerHTML = 'Your browser does not appear to <br> support the Web Animation API';
                    document.querySelector('h2').innerHTML = 'So you see a grid of items like this';
                }

                // pretty self-explanatory
                function playMarquee() {
                    if (marquee.playState === 'paused') marquee.play();
                }

                // pretty self-explanatory
                function pauseMarquee() {
                    if (marquee.playState === 'running') marquee.pause();
                }

                function debounce(func) {
                    var scheduled, context, args;
                    return function () {
                        context = this; args = [];
                        for (var i = 0; i < arguments.length; ++i) args[i] = arguments[i];
                        !!scheduled && window.cancelAnimationFrame(scheduled);
                        scheduled = window.requestAnimationFrame(function () {
                            func.apply(context, args);
                            scheduled = null;
                        });
                    }
                }
            })();
    </script>

    <script type="text/javascript">
        (function () {

            // we need to set this here for now.
            var marquee;

            // first, let's grab the element we're going to move around
            var marquee_el = document.querySelector('.carouselPre1');
            var children = marquee_el.querySelectorAll('.itemsC');

            function createMarquee() {

                if ('animate' in marquee_el && typeof marquee_el.animate === 'function') {

                    if (typeof marquee !== 'undefined') marquee.cancel();

                    marquee_el.style.whiteSpace = 'nowrap';

                    var displacement = 0;

                    for (var j = 0; j < children.length; ++j) displacement += children[j].clientWidth;

                    displacement = (displacement - marquee_el.clientWidth) << 0;

                    marquee = marquee_el.animate([

                        { transform: 'matrix(1, 0.00, 0.00, 1, 0, 0)', offset: 0 },
                        { transform: 'matrix(1, 0.00, 0.00, 1,' + -displacement + ', 0)', offset: 1 }

                    ],
                        {

                            duration: children.length * 1e3,

                            easing: 'linear',

                            delay: 0,

                            // kind of crucial for a marquee...
                            iterations: Infinity,

                            // invert animation after completion, so it scrolls backwards */
                            direction: 'alternate',

                            fill: 'forwards'
                        });
                }
            }


            if ('animate' in marquee_el && typeof marquee_el.animate === 'function') {

                // okay, let's fire up the marquee!
                createMarquee();

                // now for the playing/pausing
                marquee_el.addEventListener('mouseenter', pauseMarquee, false);
                marquee_el.addEventListener('mouseleave', playMarquee, false);

                // and resizing
                window.addEventListener('resize', debounce(createMarquee), false);

            } else {

                document.querySelector('h1').innerHTML = 'Your browser does not appear to <br> support the Web Animation API';
                document.querySelector('h2').innerHTML = 'So you see a grid of items like this';
            }

            // pretty self-explanatory
            function playMarquee() {
                if (marquee.playState === 'paused') marquee.play();
            }

            // pretty self-explanatory
            function pauseMarquee() {
                if (marquee.playState === 'running') marquee.pause();
            }

            function debounce(func) {
                var scheduled, context, args;
                return function () {
                    context = this; args = [];
                    for (var i = 0; i < arguments.length; ++i) args[i] = arguments[i];
                    !!scheduled && window.cancelAnimationFrame(scheduled);
                    scheduled = window.requestAnimationFrame(function () {
                        func.apply(context, args);
                        scheduled = null;
                    });
                }
            }
        })();
    </script>
            <script type="text/javascript" src="../js/flip-card.js"></script>
    </form>
    <div class="theme-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="theme-copyright-text">
                        Copyright &copy; 2021
             
                        <a href="#">Tripforo</a>. All rights reserved.
           
                    </p>
                </div>
                <div class="col-md-6">
                    <ul class="theme-copyright-social">
                        <li>
                            <a class="fa fa-facebook" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-google" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-twitter" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-youtube-play" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-instagram" href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

   






