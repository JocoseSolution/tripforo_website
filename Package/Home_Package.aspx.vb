﻿Imports System.Data
Imports System.Data.SqlClient
Imports HotelShared
Imports System.Configuration.ConfigurationManager
Imports ITZLib
Partial Class Package_Home_Package
    Inherits System.Web.UI.Page
    Dim Email As String
    Dim Password As String
    Dim FirstName As String
    Dim LastName As String
    Dim AgencyName As String
    Dim Address As String
    Dim City As String
    Dim State As String
    Dim Country As String
    Dim agnttype As String
    Dim Zip As String
    Dim Phone As String
    Public Distr As String
    Public User_Type As String
    Dim ag_type As String
    Public dis_str
    Public Dis_Air
    Dim conn As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
    'Dim con As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("MyDB").ConnectionString
    Dim Dist As String
    Dim credit_limit
    Dim Ex_Id As String
    Dim exe_dept As String
    Dim agent_type As String
    Dim agent_status As String
    Private STDom As New SqlTransactionDom()
    Private ST As New SqlTransaction()
    Private sttusobj As New Status()
    Dim clsCorp As New ClsCorporate()
    Private objHTLDAL As New HotelDAL.HotelDA()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        ShowLeftPopularPkg()
                ShowRightPopularPkg()
                ShowDomesticPopularPackage()
                ShowInternationalPopularPackage()
                ShowDomesticPackage()
                ShowInternationalPackage()




    End Sub



    Public Sub ShowDomesticPackage()
        Dim Connection As New SqlConnection(conn)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetDomescticPackage", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Visible_Domestic.Visible = True
            End If
            ' Visible_Domestic.Visible = True
            PackageDomestic.DataSource = dt
            PackageDomestic.DataBind()
            Connection.Close()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub ShowInternationalPackage()
        Dim Connection As New SqlConnection(conn)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetInternationalPackage", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Visible_International.Visible = True
            End If
            'Visible_International.Visible = True
            PkgInternational.DataSource = dt
            PkgInternational.DataBind()
            Connection.Close()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Sub ShowDomesticPopularPackage()
        Dim Connection As New SqlConnection(conn)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetDomesticPopularPackageDetails", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            Repeater1.DataSource = dt
            Repeater1.DataBind()
            Connection.Close()

        Catch ex As Exception
        End Try
    End Sub
    Public Sub ShowInternationalPopularPackage()
        Dim Connection As New SqlConnection(conn)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetInternationalPopularPackage", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            Repeater2.DataSource = dt
            Repeater2.DataBind()
            Connection.Close()

        Catch ex As Exception
        End Try
    End Sub

    Public Sub ShowLeftPopularPkg()
        Dim Connection As New SqlConnection(conn)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetLeftPackage", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            LeftPkg.DataSource = dt
            LeftPkg.DataBind()
            Connection.Close()

        Catch ex As Exception
        End Try
    End Sub

    Public Sub ShowRightPopularPkg()
        Dim Connection As New SqlConnection(conn)
        Try
            Connection.Open()
            Dim dt As DataTable = New DataTable()
            Dim cmd As SqlCommand = New SqlCommand("GetRightPopularPkg", Connection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            da.Fill(dt)

            RightPkg.DataSource = dt
            RightPkg.DataBind()
            Connection.Close()

        Catch ex As Exception
        End Try
    End Sub
End Class
