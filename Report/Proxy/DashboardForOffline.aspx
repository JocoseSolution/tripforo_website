﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="DashboardForOffline.aspx.cs" Inherits="Report_Proxy_DashboardForOffline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        a {
            color: #fff;
        }
       /* .page-title {
    color: #343a40;
    font-size: 1.125rem;
    margin-bottom: 0;
}
        .page-title .page-title-icon {
    display: inline-block;
    width: 36px;
    height: 36px;
    border-radius: 4px;
    text-align: center;
    -webkit-box-shadow: 0px 3px 8.3px 0.7px rgb(163 93 255 / 35%);
    box-shadow: 0px 3px 8.3px 0.7px rgb(163 93 255 / 35%);
}
        .bg-gradient-primary {
    background: -webkit-gradient(linear, left top, right top, from(#475c7d), to(#223045)) !important;
    background: linear-gradient(to right, #475c7d, #223045) !important;
}*/
    </style>



    <script type="text/javascript">
        $(document).ready(function () {
            var setinterval = setInterval("reloadpage()", 120000);
        })

        function reloadpage() {
            location.reload();
        }

    </script>
     <ol class="breadcrumb-arrow">
        <li><a href="/Search.aspx"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Flight</a></li>
        <li><a href="DashboardForOffline.aspx">Offline Request</a></li>
                <%-- <li><a href="Proxy.aspx">Genrate Request</a></li>

        <li><a href="ProxyPaxUpdate.aspx">Update Request</a></li>--%>
       <%--<li class="dropdown">
                <a href="#lala" class="dropdown-toggle" data-toggle="dropdown" >
                   Manage Booking
                   <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" role="listbox">
                   <li class="divider"></li>
                   <li><a href="Proxy.aspx" role="option">View Request</a></li>
                   <li class="divider"></li>
                   <li><a href="ProxyPaxUpdate.aspx" role="option">Update Request</a></li>
                </ul>
             </li>--%>

    </ol>
    <div class="container-fluid page-body-wrapper">
        <div class="main-panel">
            <div class="content-wrapper">
               
               
                <div class="row">
                    <div class="col-md-3 stretch-card grid-margin">
                        <a href="/Report/Proxy/Proxy.aspx">
                            <div class="card bg-gradient-danger card-img-holder text-white">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Genrate Request<i class="mdi mdi-chart-line mdi-24px float-right"></i>
                                    </h4>
                                    
                                </div>

                            </div>
                        </a>
                    </div>

                    <div class="col-md-3 stretch-card grid-margin">
                        <a href="/Report/Proxy/ProxyPaxUpdate.aspx">
                            <div class="card bg-gradient-danger card-img-holder text-white">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Update Request<i class="mdi mdi-chart-line mdi-24px float-right"></i>
                                    </h4>
                                    <h2>
                                </div>

                            </div>
                        </a>
                    </div>

                    <div class="col-md-3 stretch-card grid-margin">
                        <a href="/Report/Proxy/ProxyPaxUpdate.aspx?status=open">
                            <div class="card bg-gradient-danger card-img-holder text-white">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Open<i class="mdi mdi-chart-line mdi-24px float-right"></i>
                                    </h4>
                                    <h2>
                                        <asp:Label ID="open" runat="server" Text=""></asp:Label></h2>
                                </div>

                            </div>
                        </a>
                    </div>

                    <div class="col-md-3 stretch-card grid-margin">
                        <div class="card bg-gradient-info card-img-holder text-white">
                            <a href="/Report/Proxy/ProxyPaxUpdate.aspx?status=quote">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Quote<i class="mdi mdi-bookmark-outline mdi-24px float-right"></i>
                                    </h4>
                                    <h2>
                                        <asp:Label ID="quote" runat="server" Text=""></asp:Label></h2>

                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 stretch-card grid-margin">
                        <div class="card bg-gradient-success card-img-holder text-white">
                            <a href="/Report/Proxy/ProxyPaxUpdate.aspx?status=quoteAccepted">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Quote Accepted<i class="mdi mdi-diamond mdi-24px float-right"></i>
                                    </h4>
                                    <h2>
                                        <asp:Label ID="quoteAccepted" runat="server" Text=""></asp:Label></h2>
                                    <%--<h6 class="card-text">Increased by 5%</h6>--%>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 stretch-card grid-margin">
                        <div class="card bg-gradient-success card-img-holder text-white">
                            <a href="/Report/Proxy/ProxyPaxUpdate.aspx?status=quoteRejected">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Quote Rejected<i class="mdi mdi-diamond mdi-24px float-right"></i>
                                    </h4>
                                    <h2>
                                        <asp:Label ID="quoteRejected" runat="server" Text=""></asp:Label></h2>
                                    <%-- <h6 class="card-text">Increased by 5%</h6>--%>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 stretch-card grid-margin">
                        <div class="card bg-gradient-success card-img-holder text-white">
                            <a href="/Report/Proxy/ProxyPaxUpdate.aspx?status=namegiven">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Names Given<i class="mdi mdi-diamond mdi-24px float-right"></i>
                                    </h4>
                                    <h2>
                                        <asp:Label ID="NameGiven" runat="server" Text=""></asp:Label></h2>
                                    <%-- <h6 class="card-text">Increased by 5%</h6>--%>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 stretch-card grid-margin">
                        <div class="card bg-gradient-success card-img-holder text-white">
                            <a href="/Report/Proxy/ProxyPaxUpdate.aspx?status=cancelled">
                                <div class="card-body">
                                    <img src="https://www.bootstrapdash.com/demo/purple/jquery/template/assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Request Cancelled<i class="mdi mdi-diamond mdi-24px float-right"></i>
                                    </h4>
                                    <h2>
                                        <asp:Label ID="RquestCancelled" runat="server" Text=""></asp:Label></h2>
                                    <%-- <h6 class="card-text">Increased by 5%</h6>--%>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</asp:Content>

